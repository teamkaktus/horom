<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Category;
use app\modules\administration\models\Pricing;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;

class CategoryController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        $this->enableCsrfValidation = false;
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {            
        $modelNewCategory = new Category;
        $modelNewCategory->scenario = 'add';
        if(Yii::$app->request->post()){
            if($modelNewCategory->load(\Yii::$app->request->post())){
                if($modelNewCategory->save()){
                    $modelNewCategory = new Category;
                    $modelNewCategory->scenario = 'add';
                    \Yii::$app->session->setFlash('category_added');
                }else{
                    \Yii::$app->session->setFlash('category_not_added');                    
                }
            }
        }
        $arrayParentCategory = \yii\helpers\ArrayHelper::map(Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all(), 'id', 'name');
        
        $queryCategories = Category::find();
        $modelCategories = new ActiveDataProvider(['query' => $queryCategories, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('index', [
            'arrayParentCategory' => $arrayParentCategory,
            'modelNewCategory' => $modelNewCategory,
            'modelCategories' => $modelCategories,
        ]);
    }
    
    public function actionDelete($id=null)
    {
        if($id != null){
            $modelCategory = Category::find()->where(['id' => $id])->one();
            if($modelCategory != null){
                if($modelCategory->delete()){
                    \Yii::$app->session->setFlash('category_deleted');
                }else{
                    \Yii::$app->session->setFlash('category_not_deleted');
                }
                return $this->redirect('/administration/category/index');
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionUpdate($id=null)
    {            
        if($id != null){
            $modelCategory = Category::find()->where(['id' => $id])->one();
            if($modelCategory != null){
                $modelCategory->scenario = 'update';

                if(Yii::$app->request->post()){
                    if($modelCategory->load(\Yii::$app->request->post())){
                        if($modelCategory->save()){
                            \Yii::$app->session->setFlash('category_updated');
                        }else{
                            \Yii::$app->session->setFlash('category_not_updated');
                        }
                        return $this->redirect('/administration/category/index');
                    }
                }
                
                $arrayParentCategory = \yii\helpers\ArrayHelper::map(Category::find()->where(['parent_id' => 0])->orWhere(['parent_id' => null])->all(), 'id', 'name');
                
                return $this->render('update', [
                    'arrayParentCategory' => $arrayParentCategory,
                    'modelCategory' => $modelCategory,
                ]);
                
            }else{
                throw new \yii\web\NotFoundHttpException(); 
            }            
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionSavecategoryphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/category/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return 'images/category/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    
}
