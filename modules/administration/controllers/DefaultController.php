<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class DefaultController extends Controller
{
    public function beforeAction($action) {
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && (Yii::$app->user->identity->type == 'admin')){
            return parent::beforeAction($action);            
        }else{
            if(\Yii::$app->user->isGuest){
                return parent::beforeAction($action);            
               //return $this->redirect('administration'); 
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
        }
    }
    
    public function actionIndex()
    {
//        echo 'dfdf';exit;
        if(\Yii::$app->user->isGuest){
            
            $model = new LoginForm();
            $model->scenario = 'loginAdmin';
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if(Yii::$app->user->identity->type == 'admin'){
                    return $this->redirect('/administration/default/setting');
                }else{
                    return $this->redirect('/');                    
                }
            }
            return $this->render('index', [
                'model' => $model,
            ]);
            
        }else{
           return $this->redirect('administration/default/setting'); 
        }
        
    }
    
    public function actionSetting()
    {
        $modelNewSetting = new Setting();
        $modelNewSetting->scenario = 'add_setting';
        if($modelNewSetting->load(Yii::$app->request->post())){
            if($modelNewSetting->save()){
                Yii::$app->session->setFlash('add_setting');
            }else{
                Yii::$app->session->setFlash('not_add_setting');
            }
        }
        $querySetting = Setting::find();
        $modelSetting = new ActiveDataProvider(['query' => $querySetting, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('setting',[
            'modelSetting' => $modelSetting,
            'modelNewSetting' => $modelNewSetting
        ]);
    }

    public function actionSettingdelete($id=null)
    {
        
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        if($modelSetting->delete()){
            Yii::$app->session->setFlash('delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }else{
            Yii::$app->session->setFlash('not_delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }
    }
    
    public function actionSettingupdate($id=null)
    {
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        $modelSetting->scenario = 'update_setting';
        if($modelSetting->load(Yii::$app->request->post())){
            if($modelSetting->save()){
                Yii::$app->session->setFlash('update_setting');
                return $this->redirect(Url::home().'administration/default/setting');
            }else{
                Yii::$app->session->setFlash('not_update_setting');
            }
        }
        return $this->render('settingupdate', [
            'modelSetting' => $modelSetting,
        ]);
    }
    
    //pages begin
    public function actionPages(){        
        $modelPages = Pages::find()->all();
        return $this->render('pages', [
            'modelPages' => $modelPages,
        ]);
    }
    
    public function actionPage($id=null){
        
        $modelPage = Pages::find()->where(['id' => $id])->one();
        $modelPage->scenario = 'update';
        if($_POST){
            if($modelPage->load(Yii::$app->request->post())){
                if($modelPage->save()){
                    Yii::$app->session->setFlash('update_page');
                    return $this->redirect('/administration/default/pages');
                }else{
                    Yii::$app->session->setFlash('not_update_page');
                    return $this->redirect('/administration/default/pages');
                }
            }
        }
        
        return $this->render('page', [
            'modelPage' => $modelPage,
        ]);
    }
    //pages end
    
    
    
    
}
