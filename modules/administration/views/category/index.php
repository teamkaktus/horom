<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    <?php
        if(Yii::$app->session->hasFlash('category_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category added',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('category_not_added')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not added!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('category_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('category_not_updated')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not updated!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('category_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Category deleted',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('category_not_deleted')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Category not deleted',
                ]);
        endif; 
    ?>


<section class="content-header">
    <h1 style="color:black;">
        Categories list
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Categories list</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Category add form</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?= $form->field($modelNewCategory, 'name')->textinput(); ?>
                        <?= $form->field($modelNewCategory, 'parent_id')->dropDownList($arrayParentCategory,['prompt' => 'Pleace choise category']); ?>
                        <?= $form->field($modelNewCategory, 'img_src')->hiddeninput()->label(false); ?>
                        <img src="/images/default_avatar.jpg" class="previewCategoryImage" style="width:150px;height:150px;"><br>
                        <a class="btn btn-success addCategoryImage" >Изменить фото</a><br><br>
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="color:black;">
                    <h3 class="box-title">Pricing list</h3>
                    <div class="box-tools">
                        <div class="input-group">
                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding" style="color:black;">
                        <?= GridView::widget([
                            'dataProvider' => $modelCategories,
                            'tableOptions' => [
                                'class' => 'table table-hover'
                            ],
                            'columns' => [
                                'name',
                                [
                                    'attribute' => 'parent_category',
                                    'format' => 'html',
                                    'value' => function ($modelCategories) {
                                            if($modelCategories->parent){
                                                return $modelCategories->parent->name;                                                
                                            }
                                    }
                                ],
                                [
                                    'attribute' => 'Image',
                                    'format' => 'html',
                                    'value' => function ($modelCategories) {
                                    $image = '/images/default_avatar.jpg';
                                        if(($modelCategories['img_src'] != '') && ($modelCategories['img_src'] != null)){ 
                                            $image = '/'.$modelCategories['img_src'];
                                        } 
                                        return '<img src="'.$image.'" style="width:70px;height:70px;">';
                                    }
                                ],
                                'date_create',
                                [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{update} {delete}',
                                     'buttons' => [
                                         'delete' => function ($url,$modelPricings) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-trash"></span>', 
                                                 'delete?id='.$modelPricings['id']);
                                         },
                                         'update' => function ($url,$modelPricings) {
                                                 return Html::a(
                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                 'update?id='.$modelPricings['id']);
                                         },
                                     ],
                                 ],
                            ],
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
 
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>