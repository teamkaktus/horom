<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    
<section class="content-header">
    <h1 style="color:black;">
        Category Update
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category update</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6" style="margin:0 auto;float:none;">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Category update form</h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body" style="color:black;">
                        <?= $form->field($modelCategory, 'name')->textinput(); ?>                        
                        <?= $form->field($modelCategory, 'parent_id')->dropDownList($arrayParentCategory, ['prompt' => 'Pleace choise category']); ?>
                        
                        <?= $form->field($modelCategory, 'img_src')->hiddeninput()->label(false); ?>
                        
                        <?php $image = '/images/default_avatar.jpg' ?>
                        <?php if(($modelCategory->img_src != '') && ($modelCategory->img_src != null)){ ?>
                            <?php $image = '/'.$modelCategory->img_src; ?>                            
                        <?php } ?>
                        <img src="<?= $image; ?>" class="previewCategoryImage" style="width:150px;height:150px;"><br>
                        <a class="btn btn-success addCategoryImage" >Изменить фото</a><br><br>
                        
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>                                    
                <!-- form end -->

            </div>
        </div>
    </div>
</section>

 <div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>