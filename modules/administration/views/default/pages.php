<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<section class="content-header">
                    <h1 style="color:black;">
                        Site setting
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Site setting</li>
                    </ol>
                </section>
                
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header" style="color:black;">
                                    <h3 class="box-title">Responsive Hover Table</h3>
                                    <div class="box-tools">
                                        <div class="input-group">
                                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding" style="color:black;">
                                        <table class="table">
                                            <tr>
                                                <td><b>#</b></td>
                                                <td><b>Страница</b></td>
                                                <td><b>Сортировка</b></td>
                                                <td></td>
                                            </tr>
                                            <?php foreach($modelPages as $page){ ?>
                                                <tr>
                                                    <td><?= $page['id']; ?></td>
                                                    <td><?= $page['name']; ?></td>
                                                    <td><?= $page['sort']; ?></td>
                                                    <td><a href="<?= Url::home(); ?>administration/default/page/<?= $page['id']; ?>" class="btn btn-primary">Update</a></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>