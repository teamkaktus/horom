<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<div class="form-box" id="login-box">
        <div class="header">Логин</div>
        <?php $form = ActiveForm::begin([]); ?>
            <div class="body bg-gray">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput()->label(false); ?>
                 <?= $form->field($model, 'rememberMe')->checkbox([
                    'template' => "<div>{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                ]) ?>
            </div>
            <div class="footer" style="height: auto">
                <?= Html::submitButton('Sign me in', ['class' => 'btn bg-olive btn-block', 'name' => 'login-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
