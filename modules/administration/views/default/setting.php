<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

?>
    <?php
        if(Yii::$app->session->hasFlash('update_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Данные изменены',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('add_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Данные добавлени!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('not_add_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Данные не добавлени!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('delete_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Данные удалени!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('not_delete_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Данные не удалени!',
                ]);
        endif; 
    ?>
                <section class="content-header">
                    <h1 style="color:black;">
                        Site setting
                        <small>Preview</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Site setting</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6" style="margin:0 auto;float:none;">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Quick Example</h3>
                                </div><!-- /.box-header -->
                                
                                <!-- form start -->
                                    <?php $form = ActiveForm::begin(); ?>
                                    <div class="box-body" style="color:black;">
                                        <?= $form->field($modelNewSetting, 'key')->textinput(); ?>
                                        <?= $form->field($modelNewSetting, 'name')->textinput(); ?>
                                        <?= $form->field($modelNewSetting, 'value')->textinput(); ?>
                                                                        
                                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>                                    
                                <!-- form end -->
                                
                            </div>
                        </div>
                    </div>
                </section>
                
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header" style="color:black;">
                                    <h3 class="box-title">Responsive Hover Table</h3>
                                    <div class="box-tools">
                                        <div class="input-group">
                                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding" style="color:black;">
                                        <?= GridView::widget([
                                            'dataProvider' => $modelSetting,
                                            'tableOptions' => [
                                                'class' => 'table table-hover'
                                            ],
                                            'columns' => [
                                                'key',
                                                'name',
                                                'value',
                                                [
                                                     'class' => 'yii\grid\ActionColumn',
                                                     'template' => '{delete} {update}',
                                                     'buttons' => [
                                                         'delete' => function ($url,$modelSetting) {
                                                                 return Html::a(
                                                                 '<span class="glyphicon glyphicon-trash"></span>', 
                                                                 'settingdelete?id='.$modelSetting['id']);
                                                         },
                                                         'update' => function ($url,$modelSetting) {
                                                                 return Html::a(
                                                                 '<span class="glyphicon glyphicon-pencil"></span>', 
                                                                 'settingupdate?id='.$modelSetting['id']);
                                                         },
                                                     ],
                                                 ],
                                            ],
                                        ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>