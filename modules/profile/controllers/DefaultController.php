<?php
namespace app\modules\profile\controllers;

use yii;
use yii\web\Controller;
use app\models\User;
use app\models\Wishlist;
use app\models\UserInfo;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();

        return $this->render('index', [
                'modelUser' => $modelUser,
        ]);
    }

    public function actionWichlist()
    {
        $id = Yii::$app->user->identity->userinfo->id;
        $modelWish = Wishlist::find()
            ->joinWith(['product'])
            ->select(['*'])
            ->where(['product_wish_list.id_user' => $id])
            ->all();
       $modelWishCount = Wishlist::find()
            ->joinWith(['product'])
            ->select(['*'])
            ->where(['product_wish_list.id_user' => $id])
            ->count();
        
        

        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $stringMyProductCount = $modelWishCount;
      
        return $this->render('myproduct', [
                'modelUser' => $modelUser,
                'stringMyProductCount' => $stringMyProductCount,
                'modelProduct' => $modelWish,
                'pagination' => $modelProduct->pagination,
        ]);

        var_dump($modelWish);
        /*
          return $this->render('wichlist', [
          'modelUser' => $modelUser,
          ]);
         */
    }

    public function actionUpdate()
    {
        $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();
        $modelUser->scenario = 'update';
        $modelUserInfo = UserInfo::find()->where(['user_id' => $modelUser->id])->one();
        if ($modelUserInfo == null) {
            $modelUserInfo = new UserInfo();
            $modelUserInfo->user_id = \Yii::$app->user->id;
        }
        $modelUserInfo->scenario = 'add';

        if (\Yii::$app->request->post()) {
            if ($modelUser->load(\Yii::$app->request->post())) {
                if ($modelUser->save()) {
                    if ($modelUserInfo->load(\Yii::$app->request->post())) {
                        if ($modelUserInfo->save()) {
                            \Yii::$app->session->setFlash('data_saved');
                        } else {
                            \Yii::$app->session->setFlash('data_not_saved');
                        }
                    }
                    $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();
                } else {
                    \Yii::$app->session->setFlash('data_not_saved');
                }
            }
        }

        $modelUserInfo = UserInfo::find()->where(['user_id' => $modelUser->id])->one();
        return $this->render('update', [
                'modelUser' => $modelUser,
        ]);
    }

    public function actionSaveprofilephoto()
    {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot') . '/images/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if (!is_dir($storeFolder . $ds . Yii::$app->user->id)) {
                mkdir($storeFolder . $ds . Yii::$app->user->id, 0777, true);
            }

            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath = $storeFolder . $ds . Yii::$app->user->id . $ds;  //4
            $for_name = time();
            $randNumber = rand(10, 99) . rand(10, 99);
            $targetFile = $targetPath . $for_name . '_' . $randNumber . '.png';  //5

            move_uploaded_file($tempFile, $targetFile); //6
            return 'images/users_images/' . \Yii::$app->user->id . '/' . $for_name . '_' . $randNumber . '.png'; //5
        }
    }

    public function actionUpdateprofilephoto()
    {
        $result = [];
        $modelUserInfo = UserInfo::find()->where(['user_id' => \Yii::$app->user->id])->one();
        $modelUserInfo->scenario = 'avatar';
        $modelUserInfo->avatar = $_POST['image_src'];
        if ($modelUserInfo->save()) {
            $result['status'] = 'success';
        } else {
            $result['status'] = 'error';
        }

        echo json_encode($result);
    }
}
