<?php

namespace app\modules\profile\controllers;
use yii;
use yii\web\Controller;
use app\models\User;
use app\models\Product;
use app\models\ProductImage;
use app\models\Category;
use app\models\UserInfo;
use app\models\News;
use app\models\ProductCategory;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `profile` module
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        
    }
    public function actionMynews(){
        $modelNewNews = new News();
        $modelNewNews->scenario = 'add_news';
        
        if(Yii::$app->request->post()){
            if($modelNewNews->load(\Yii::$app->request->post())){
                if($modelNewNews->save()){
                    \Yii::$app->session->setFlash('news_added');
                    return $this->redirect('/profile/news/mynews');
                }else{
                    \Yii::$app->session->setFlash('news_not_added');                    
                }
            }
        }
        
        
        $user_id = \Yii::$app->user->id;
        $modelNews = News::find()->where(['user_id' => $user_id])->all();
        $modelUser = User::find()->where(['id' => $user_id])->one();
        
        return $this->render('mynews',[
            'modelNews' => $modelNews,
            'modelUser' => $modelUser,
            'modelNewNews' => $modelNewNews,
        ]);
    }
    
    public function actionSavenewsphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;
        $storeFolder = \Yii::getAlias('@webroot').'/images/news/';

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return 'images/news/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
}
