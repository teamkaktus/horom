<?php

namespace app\modules\profile\controllers;
use yii;
use yii\web\Controller;
use app\models\User;
use app\models\Product;
use app\models\ProductImage;
use app\models\Category;
use app\models\UserInfo;
use app\models\ProductCategory;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `profile` module
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        $modelUser = User::find()->with('userinfo')->where(['id' => \Yii::$app->user->id])->one();
        
        return $this->render('index',[
            'modelUser' => $modelUser,
        ]);
    }
    
    public function actionNewproduct()
    {
        $modelNewProduct = new Product();
        $modelNewProduct->scenario = 'new_product';
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        if(Yii::$app->request->post()){
            if($modelNewProduct->load(\Yii::$app->request->post())){
                $modelNewProduct->user_id = \Yii::$app->user->id;
                if($modelNewProduct->save()){
                    
                    if(($modelNewProduct->category_id != '') && ($modelNewProduct->category_id != null)){
                        $modelProducCategory = new ProductCategory();
                        $modelProducCategory->scenario = 'add';
                        $modelProducCategory->product_id = $modelNewProduct->id;
                        $modelProducCategory->category_id = $modelNewProduct->category_id;
                        $modelProducCategory->save();
                    }
                    if(($_POST['Product']['sub_category'] != '') && ($_POST['Product']['sub_category'] != null)){
                        $modelProducCategory = new ProductCategory();
                        $modelProducCategory->scenario = 'add';
                        $modelProducCategory->product_id = $modelNewProduct->id;
                        $modelProducCategory->category_id = $_POST['Product']['sub_category'];
                        $modelProducCategory->save();
                    }
                    
                    $arrayProductImage = json_decode($_POST['Product']['image_array']);
                    if($arrayProductImage){
                        foreach($arrayProductImage as $key => $stringImageSrc){
                            if($key == 0){
                                $modelNewProduct->scenario = 'update_image';
                                $modelNewProduct->img_src = $stringImageSrc;
                                $modelNewProduct->save();
                            }else{
                                $modelNewProductImage = new ProductImage();
                                $modelNewProductImage->scenario = 'new_product_image';
                                $modelNewProductImage->img_src = $stringImageSrc;
                                $modelNewProductImage->product_id = $modelNewProduct->id;
                                $modelNewProductImage->save();
                            }
                        }
                    }
                    \Yii::$app->session->setFlash('product_added');
                    return $this->redirect('/profile/product/myproduct');
                }else{
                    \Yii::$app->session->setFlash('product_not_added');                    
                }
            }
        }
        $arrayCategory = ArrayHelper::map(Category::find()->where(['parent_id' => '0'])->orWhere(['parent_id' => null])->all(), 'id', 'name');
        return $this->render('newproduct',[
            'modelNewProduct' => $modelNewProduct,
            'arrayCategory' => $arrayCategory,
            'modelUser' => $modelUser,
        ]);
    }
    
    public function actionMyproduct(){
        $queryProduct = Product::find()->where(['user_id' => \Yii::$app->user->id]);
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $stringMyProductCount = $queryProduct->count();
        $modelProduct = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('myproduct',[
            'modelUser' => $modelUser,
            'stringMyProductCount' => $stringMyProductCount,
            'modelProduct' => $modelProduct->getModels(),
            'pagination' => $modelProduct->pagination,
        ]);
    }
    
    public function actionSaveproductimg(){
        $ds          = DIRECTORY_SEPARATOR;
        $storeFolder = \Yii::getAlias('@webroot').'/images/product/';

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;

            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.jpg';

            move_uploaded_file($tempFile,$targetFile);
            return \Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.jpg';
        }
    }
    
    
    public function actionDeleteproduct($id=null)
    {
        if($id != null){
            $modelProduct = Product::find()->where(['id' => $id])->one();
            if(($modelProduct) && ($modelProduct->user_id == \Yii::$app->user->id)){ 
                ProductImage::deleteAll(['product_id' => $modelProduct->id]);
                if($modelProduct->delete()){
                    \Yii::$app->session->setFlash('product_deleted');
                    return $this->redirect('/profile/product/myproduct');
                }else{
                    \Yii::$app->session->setFlash('product_not_deleted');
                    return $this->redirect('/profile/product/myproduct');                    
                }
            }else{
                throw new \yii\web\NotFoundHttpException();                 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }          
    }            
    public function actionUpdateproduct($id=null)
    {
        if($id != null){            
            $modelProduct = Product::find()->where(['id' => $id])->one();
            if(($modelProduct) && ($modelProduct->user_id == \Yii::$app->user->id)){                
                    $modelProduct->scenario = 'new_product';

                    if(Yii::$app->request->post()){
                        if($modelProduct->load(\Yii::$app->request->post())){
                            $modelProduct->user_id = \Yii::$app->user->id;
                            if($modelProduct->save()){
                                ProductImage::deleteAll(['product_id' => $modelProduct->id]);
                                $arrayProductImage = json_decode($_POST['Product']['image_array']);                        
                                if($arrayProductImage){
                                    foreach($arrayProductImage as $key => $stringImageSrc){
                                        if($key == 0){
                                            $modelProduct->scenario = 'update_image';
                                            $modelProduct->img_src = $stringImageSrc;
                                            $modelProduct->save();
                                        }else{
                                            $modelNewProductImage = new ProductImage();
                                            $modelNewProductImage->scenario = 'new_product_image';
                                            $modelNewProductImage->img_src = $stringImageSrc;
                                            $modelNewProductImage->product_id = $modelProduct->id;
                                            $modelNewProductImage->save();
                                        }
                                    }
                                }

                            }
                        }
                    }
                    
                    $arrayProductType = ArrayHelper::map(Category::find()->where(['parent_id' => '0'])->orWhere(['parent_id' => null])->all(), 'id', 'name');
                    $arrayProductImage = [];                    
                    if(($modelProduct->img_src != '') && ($modelProduct->img_src != null)){
                        $arrayProductImage[] = $modelProduct->img_src;
                    }
                    foreach($modelProduct->images as $productImage){
                        $arrayProductImage[] = $productImage['img_src'];                        
                    }
                    $jsonProductImage = json_encode($arrayProductImage);
                    
                    return $this->render('updateproduct',[
                        'modelProduct' => $modelProduct,
                        'arrayProductType' => $arrayProductType,
                        'jsonProductImage' => $jsonProductImage,
                    ]);
            }else{
                throw new \yii\web\NotFoundHttpException();                 
            }
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    
    public function actionSaveprofilephoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.png';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return 'images/users_images/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.png'; //5
        }
    }
    
    
}
