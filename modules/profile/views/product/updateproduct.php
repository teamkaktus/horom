<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    
    $this->title = 'Profile new product';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="investicii">
    <div class="hidden-xs container">
        <div class="col-xs-12 padd-zero" style="padding-top: 10px !important;">
                <?php $this->params['breadcrumbs'][] = $this->title; ?>
        </div>
    </div>
    <?= $this->render('/default/menu'); ?>
    
    <?php $form = ActiveForm::begin(); ?>
        <div class="container clas_cabin_no_conteiner22">
            <div class="di_mar_top2"><span class="sp_black">Редактировать объявление</span></div>
            <div class="col-sm-12 pad_lef_0">
                <div class="col-md-9 col-sm-8 no-padding no-padding444">
                    <div class="col-lg-3 col-md-4 col-sm-4 otstup22 otstup4_text">
                            <?= $form->field($modelProduct, 'product_type_id')->dropDownList($arrayProductType,['class' => 'form-style select-style form_style_tup']); ?>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 otstup33 otstup4_text">
                        <?= $form->field($modelProduct, 'object_id')->dropDownList(['Коттедж'],['class' => 'form-style select-style form_style_tup']); ?>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-4 otstup33 otstup4_text">
                        <?= $form->field($modelProduct, 'name')->textinput(['class' => 'form-style form_style_tup']); ?>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 otstup22 otstup455 otstup4_text">
                        <?= $form->field($modelProduct, 'country_id')->dropDownList(['Италия'],['class' => 'form-style select-style form_style_tup']); ?>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 otstup44 otstup4_text">
                        <?= $form->field($modelProduct, 'region_id')->dropDownList(['Bassilicata'],['class' => 'form-style select-style form_style_tup']); ?>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 otstup44 otstup4_text">
                        <?= $form->field($modelProduct, 'city_id')->dropDownList(['Rome'],['class' => 'form-style select-style form_style_tup']); ?>
                        
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 no-padding no-padding333 otstup4_text">
                    <?= $form->field($modelProduct, 'about')->textarea(['class' => 'textte_opus2 form-style']); ?>
                </div>
            </div>
        </div>
        <div class="bord_bot_22"></div>
        <input type="hidden" id="product-image_array" name="Product[image_array]" value='<?= $jsonProductImage; ?>'>
        <div class="container clas_cabin_no_conteiner22">
            <div class="col-xs-12 no-padding no_padddd">
                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding no_padddd">
                    <div class="mar_tex_pod"><span class="sp_tex_pod">Загрузите фото:</span></div>
                    <div style="clear: both"></div>
                    <div class="owl-carousel owl-theme">
                        
                        <div class="item_img33" style='height:auto;padding:5px;'>
                            <div>
                                Основное фото:
                            </div>
                            <div style="position:relative;">
                                <div class='NewProductImageBlock addPhoto' data-delete_status="yes" style='background-image: url(/images/product/<?= $modelProduct['img_src']; ?>);'>
                                </div>
                                <div class="deleteProductImage" data-name="<?= $modelProduct['img_src'];  ?>" style='display:none;'> 
                                    <span>Удалить</span>
                                </div>
                            </div>
                        </div>
                        <?php foreach($modelProduct->images as $key => $image){ ?>
                            <div class="item_img33" style='height:auto;padding:5px;'>
                                <div>
                                    Фото <?= $key+2; ?>:
                                </div>
                                <div style="position:relative;">
                                    <div class='NewProductImageBlock addPhoto' data-delete_status="yes" style='background-image: url(/images/product/<?= $image['img_src']; ?>);'>
                                    </div>
                                    <div class="deleteProductImage" data-name="<?= $image['img_src']; ?>" style='display:none;'> 
                                        <span>Удалить</span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        
                        <?php $countProductImage = count($modelProduct->images); ?>
                        <?php $countP = 9 - (int)$countProductImage; ?>
                        <?php for ($i = 1; $i <= $countP+1; $i++) { ?>
                            <div class="item_img33" style='height:auto;padding:5px;'>
                                <div>
                                    Фото <?= $i+1; ?>:
                                </div>
                                <div style="position:relative;">
                                    <div class='NewProductImageBlock addPhoto' data-delete_status="no" style='background-image: url(/images/default_avatar.jpg);'>
                                    </div>
                                    <div class="deleteProductImage" data-name="" style='display:none;'>
                                        <span>Удалить</span>
                                    </div>
                                </div>
                            </div> 
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="bord_bot_b_33"></div>
        <div class="container clas_cabin_no_conteiner22">
            <div class="col-sm-12 pad_lef_555">
                <div class="col-sm-9 no-padding">
                    <div class="col-md-3 col-sm-3 otstup22">
                        <?= $form->field($modelProduct, 'object_area')->textinput(['class' => 'form-style form_style_tup']); ?>
                    </div>
                    <div class="col-md-6 col-sm-6 otstup33">
                        <?= $form->field($modelProduct, 'address')->textinput(['class' => 'form-style form_style_tup']); ?>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12 no-padding">
                    <div class="col-md-6 col-sm-5 col-xs-12 otstup_123 otstup33">
                        <?=
                            $form->field($modelProduct, 'address_status')
                                ->radioList(
                                    [1 => 'Да', 0 => 'Нет'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            $check = '';
                                            if($checked == 1){
                                                $check = 'checked';                                                
                                            }
                                            $return = '<input type="radio" name="' . $name . '" '.$check.' value="' . $value . '" tabindex="3">';
                                            $return .= '<label>' . ucwords($label) . '</label>';
                                            return $return;
                                        }
                                    ]
                                );
                        ?>
                    </div>
                    <div class="col-md-6 col-sm-5 col-xs-12 otstup_233 otstup33">
                        <?= $form->field($modelProduct, 'year_of_construction')->textinput(['class' => 'form-style form_style_tup form_style_tup222']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="bord_bot_b_444"></div>
        <div class="container clas_cabin_no_conteiner22">
            <div class="col-sm-12 pad_lef_555">
                <div class="col-sm-9 no-padding">
                    <div class="col-md-3 col-sm-3 otstup22">
                        <?= $form->field($modelProduct, 'price')->textinput(['class' => 'form-style form_style_tup']); ?>

                    </div>
                    <div class="col-md-3 col-sm-3 otstup22">
                        <?= $form->field($modelProduct, 'currency')->dropDownList([0 => 'Euro', 1 => 'Dollar', 2 => 'Рубль'],['class' => 'form-style select-style form_style_tup']); ?>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="bord_bot_b_444"></div>
        <div class="container clas_cabin_no_conteiner22">
            <div class="col-sm-12 pad_lef_555">
                <div class="col-md-9 col-sm-11 col-xs-12 no-padding">
                    <div class="col-md-4 col-sm-4 col-xs-12 otstup22 ots_mar_45">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить объявление'), ['class' => 'btn bt_bot_bb_3']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>   
</div>
                

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>