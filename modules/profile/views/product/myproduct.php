<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    
    $this->title = 'Мои объявления';
?>
 <?php
    if(Yii::$app->session->hasFlash('product_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления добавлено',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('product_deleted')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления удалино',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('product_not_deleted')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления не удалино',
        ]);
    endif;
?>
<?= $this->render('/default/menu'); ?>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('/default/user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="description_user">
                <?php var_dump($modelProduct); ?>
            </div>
        </section>
    </div>
</section>