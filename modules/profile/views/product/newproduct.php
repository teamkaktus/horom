<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use dosamigos\datepicker\DatePicker;
    
    $this->title = 'Profile new product';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('/default/menu'); ?>
 <?php
    if(Yii::$app->session->hasFlash('product_not_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-error',
                ],
                'body' => 'Объявления не добавлено',
        ]);
    endif;
?>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('/default/user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="my_orders">
                    <h2>Заявка на открытие закупки</h2>
                    
                    <span class="title-h4-style">Заполните поля для подачи заявки</span>
                    <div class="content-style">
                        <?php $form = ActiveForm::begin(); ?>
                                <div class="content-top-div-style">
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'name')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'brand')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'price')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'type')->dropDownList(['0' => 'Закуп', '1' => 'Товар'],['class' => 'styled-select','style' => 'width:100%;']); ?>                                        
                                    </div>
                                </div>
                                <div class="content-top-div-style">
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'supplier')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'supplier_site')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'product_count')->input('number',['class' => 'form-input-style']); ?>
                                    </div>
                                </div>
                                <div class="content-middle-div-style" style="width: 100%">
                                    <div class="form-group-style form-label-style"  style="width: 100%">
                                        <?= $form->field($modelNewProduct, 'category_id')->dropDownList($arrayCategory, ['prompt' => 'Pleace choise category','class' => 'styled-select','style' => 'width:100%;']); ?>                                        
                                    </div>
                                    <div class="form-group-style form-label-style" id="block-sub_category"  style="width: 100%;display:none;">
                                        <?= $form->field($modelNewProduct, 'sub_category')->dropDownList([], ['prompt' => 'Pleace choise category','class' => 'styled-select','style' => 'width:100%;']); ?>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                
                                <?= $form->field($modelNewProduct, 'image_array')->hiddeninput(); ?>
                                <span class="addPhoto btn btn-primary">Add photo</span>
                                <div id="previewsFotoContainer"></div>
                                
                                
                                <div class="content-middle-div-style">
                                    <div class="content-date-div-style">
                                        <?= $form->field($modelNewProduct, 'date_start')->widget(
                                            DatePicker::className(), [
                                                'clientOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd-mm-yyyy'
                                                ]
                                        ]);?>
                                    </div>
                                    <div class="content-date-div-style_1">
                                        <?= $form->field($modelNewProduct, 'date_end')->widget(
                                            DatePicker::className(), [
                                                'clientOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'dd-mm-yyyy'
                                                ]
                                        ]);?>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                    <div class="form-group-style form-label-style">
                                        <?= $form->field($modelNewProduct, 'short_description')->textinput(['class' => 'form-input-style']); ?>
                                    </div>
                                <div class="content-bottom-div-style">
                                    <div class="form-label-style">
                                        <label>Комментарий к закупке</label>
                                    </div>
                                    <?= $form->field($modelNewProduct, 'about')->textarea(['class' => 'textarea-style'])->label(false); ?>
                                    
                                    <div style="clear: both"></div>
                                </div>
                                <?= Html::submitButton(Yii::t('app', 'Сохранить объявление'), ['class' => 'btn btn-success']) ?>
                                <div style="clear: both;"></div>
                        <?php ActiveForm::end(); ?>  
                    </div>
                </div>
        </section>
    </div>
</section>    
        

<div style="display:none;">
    
    <div class="table table-striped" class="files" id="previews">
        <div id="template" class="file-row row">
            <div class="col-sm-3">
                <span class="preview"><img data-dz-thumbnail /></span>
            </div>
            <div class="col-sm-3">
                <p class="name" data-dz-name></p>
                <strong class="error text-danger" data-dz-errormessage></strong>
            </div>
            <div class="col-sm-3">
                <p class="size" data-dz-size></p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
            <div class="col-sm-3">
              <button data-dz-remove class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
              </button>
            </div>
        </div>
    </div>
    
</div>