
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
use app\assets\ProfileAsset;
ProfileAsset::register($this);

$class = '';
?>
<?php 
    $class = ($this->context->getRoute() == 'profile/default/index')?'active':''; 
    $class1 = ($this->context->getRoute() == 'profile/default/update')?'active':''; 
    $class2 = ($this->context->getRoute() == 'profile/news/mynews')?'active':''; 
?>
<?php if(\Yii::$app->user->identity->type == 'сompetitor'){ ?>
    <section class="menu_content">
        <div class="wrapper">
            <div class="navigation_block">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Личный кабинет</a></li>
                </ul>
            </div>
            <div class="menu_profile">
                <ul>
                    <li><a href="/profile/default/index" class="<?= $class ?>">О себе</a></li>
                    <li><a href="message.html">Мои сообщения <sup><small>n</small></sup></a></li>
                    <li><a href="my_friends.html">Друзья</a></li>
                    <li><a href="/wichlist">Избранное</a></li>
                    <li><a href="my_orders.html">Мои заказы</a></li>
                    <li><a href="comments_and_responses.html">Мои комментарии и ответы</a></li>
                    <li><a href="/profile/news/mynews" class="<?= $class2; ?>">Мои новости</a></li>
                </ul>
                <div class="profile_options">
                    <a  class="<?= $class1 ?>" href="/profile/default/update">Настройки профиля</a>
                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>
    <section class="menu_content">
        <div class="wrapper">
            <div class="navigation_block">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Личный кабинет</a></li>
                </ul>
                <div class="money-user-account">
                  <strong>Баланс: 0 руб.</strong>
                  <span class="make-an-order">Пополнить баланс</span>
                </div>
            </div>
            <div class="menu_profile">
                <ul>
                    <li><a href="/profile/default/index" class="<?= $class ?>">О себе</a></li>
                    <li><a href="message.html">Мои сообщения <sup><small>n</small></sup></a></li>
                    <li><a href="/profile/product/myproduct">Мои закупи</a></li>
                    <li><a href="my_friends.html">Друзья</a></li>
                    <li><a href="/wichlist">Избранное</a></li>
                    <li><a href="my_orders.html">Мои заказы</a></li>
                    <li><a href="comments_and_responses.html">Мои комментарии и ответы</a></li>
                    <li><a href="/profile/news/mynews" class="<?= $class2; ?>">Мои новости</a></li>
                </ul>
                <div class="profile_options">
                    <a  class="<?= $class1 ?>" href="/profile/default/update">Настройки профиля</a>
                </div>
                <div class="block-management-accounts">
                  <span class=" make-an-order my-account "><i class="fa fa-database" aria-hidden="true"></i> Мои счета</span>
                  <span class="make-an-order" ><a href="/profile/product/newproduct"><i class="fa fa-plus" aria-hidden="true"></i> Подать заявку на открытие закупки</a></span>
                </div>
            </div>
        </div>
    </section>
<?php } ?>