
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
use app\assets\ProfileAsset;
ProfileAsset::register($this);

$class = '';
?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<div class="photo_user">
    <div class="avatar">
        <?php if($modelUser->userinfo)?>
        <?php $avatar = '/images/default_avatar.jpg'; ?>
        <?php if($modelUser->userinfo->avatar != ''){ ?>
            <?php $avatar = '/'.$modelUser->userinfo->avatar; ?>
        <?php } ?>
        <img class="userProfileImage" src="<?= $avatar; ?>" style="width: 100%">

        <div class="blockSavePhotoButton" style="text-align: center;display:none;">
            <input type="hidden" name="image_prodile_src" value="<?= $avatar; ?>">
            <a class="saveProfilePhoto">Сохранить</a>
        </div>
        <div class="change_photo" style="text-align:center;">
            <a class="changeProfilePhoto sp_izm_logo" >Изменить фото</a>
        </div>
    </div>
    <?php 
        switch ($modelUser->type) {
            case 'сompetitor':
                echo "Вы Соескатель";
                break;
            case 'organizer':
                echo "Вы Организатор";
                break;
            case 'admin':
                echo "Вы Администратор";
                break;

            default:
                break;
        }
    ?>
</div>