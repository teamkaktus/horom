<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    $this->title = 'Profile';
?>
<?= $this->render('menu'); ?>
<?php // $this->params['breadcrumbs'][] = $this->title; ?>
<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="description_user">
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($modelUser->userinfo, 'username')->textinput(); ?>
                    <?= $form->field($modelUser, 'password')->passwordInput(); ?>
                    <?= $form->field($modelUser, 'password_repeat')->passwordInput(); ?>
                    <?= $form->field($modelUser, 'email')->textinput(); ?>
                    <?= $form->field($modelUser->userinfo, 'phone_number')->textinput(); ?>
                    <?= $form->field($modelUser->userinfo, 'address')->textinput(); ?>
                    <?= $form->field($modelUser->userinfo, 'about')->textarea(); ?>
                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?> 
            </div>
        </section>
    </div>
</section>
