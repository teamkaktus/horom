<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    
    $this->title = 'Мои объявления';
?>
 <?php
    if(Yii::$app->session->hasFlash('product_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления добавлено',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('product_deleted')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления удалино',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('product_not_deleted')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Объявления не удалино',
        ]);
    endif;
?>
<?= $this->render('/default/menu'); ?>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('/default/user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="description_user">
                
        <?php foreach($modelProduct as $product){ ?> 
                <div class="container_tovar idtov_<?= $product->product->id; ?>" style="float:none;">
                        <div class="box_shadow_container">
                            <a href="/product/<?= $product->product->id; ?>">
                                <?php if(($product->product->img_src != '') && ($product->product->img_src != null)){ ?>
                                <img class="image_product_in_stock" src="<?= $product->product->img_src; ?>">
                                <?php }else{ ?>
                                    <img class="image_product_in_stock" src="/images/default_avatar.jpg">
                                <?php } ?>
                            </a>

                            <?= app\widgets\ChechfavoriteWidget::widget(['product_id' => $product->product->id]); ?>
                            <div class="name_container_product_in_stock">
                                <a href="/product/<?= $product->product->id; ?>">
                                    <span class="name_product_in_stock"><?= $product->product->name; ?></span>
                                </a>
                            </div>
                            <?= app\widgets\UseroncatalogWidget::widget(['user_id' => $product->product->user_id, 'type' => 'tovar']); ?>
                            <div class="data_and_price_container">
                                <div class="data_container_product_in_stock">
                                    <span class="data_text_product_in_stock"><?= $product->product->date_create; ?></span>
                                </div>
                                <div class="price_container_product_in_stock">
                                    <span class="price_text_product_in_stock"><?= $product->product->price; ?> руб</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?> 
        
               
            </div>
        </section>
    </div>
</section>