<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $this->title = 'Profile';
?>
<?= $this->render('menu'); ?>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="description_user">
                <strong class="date_registration">На сайте с <?= $modelUser->date_create; ?></strong>
                <h2><?= $modelUser->username; ?></h2>
                <!-- <a href="#" class="online">Онлайн</a> -->
                <span class="online">Онлайн</span>
                <span class="user_rating">Ваш рейтинг - <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> 0</a></span>
                <h4>О себе</h4>
                <?= $modelUser->userinfo->about; ?>
            </div>
        </section>
        <section class="contacts_user">
            <strong>Вы указали данные:</strong>
            <ul>
                <li><img src="/img/phone_contacts_user.png" alt=""> <span><strong><?= $modelUser->userinfo->phone_number; ?></strong> </span></li>
                <li><img src="/img/letter_contacts_user.png" alt=""> <span><?= $modelUser->email; ?></span></li>
                <li><img src="/img/region_contacts_user.png" alt=""> <span><?= $modelUser->userinfo->address; ?></span></li>
            </ul>
            <a href="/profile/default/update">Изменить данные</a>
        </section>
    </div>
</section>