<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use yii\helpers\StringHelper;
    
    $this->title = 'Мои объявления';
?>
 <?php
    if(Yii::$app->session->hasFlash('news_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Новость добавлена',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('news_not_added')):
        echo Alert::widget([
                'options' => [
                        'class' => 'alert-info',
                ],
                'body' => 'Новость не добавлена',
        ]);
    endif;
?>
<?= $this->render('/default/menu'); ?>
<section class="main_content">
    <div class="wrapper">
        <section class="user_profile">
            <?= $this->render('/default/user_avatar',['modelUser' => $modelUser ]); ?>                
            <div class="description_user">
                <div class="my_orders">
                    <h2>Мои новости</h2>
                    <!-- <a href="#" class="online">Онлайн</a> -->
                    <span class="online">Онлайн</span>
                    <!--<div class="admin-news-maine">
                        <div class="admin-news-div-title">
                            Новости от администрации
                        </div>
                        <div class="admin-left-news">
                            <div class="left-news-style">
                                <div class="admin-news-title">
                                    <a href="#">Вы можете стать участником акции!</a>
                                </div>
                                <div class="admin-news-content">
                                    По данным еженедельного доклада Управления по энергетической информации (EIA)
                                    при Минэнерго США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г.
                                    упал на 5,9 млн баррелей. По данным еженедельного доклада Управления по энергетической
                                    информации (EIA) при Минэнерго США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г.
                                    упал на 5,9 млн баррелей. По данным еженедельного доклада Управления по энергетической информации
                                    (EIA) при Минэнерго США (PDF), объем коммерческих
                                    США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей. По данным еженедельного
                                    доклада Управления по энергетической информации (EIA) при Минэнерго США (PDF), объем коммерческих
                                    США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                                </div>
                                <div class="admin-news-date">
                                    23:55, 28 мая, 2015
                                </div>
                            </div>
                            <div class="left-news-style">
                                <div class="admin-news-title">
                                    <a href="#">Вы можете стать участником акции!</a>
                                </div>
                                <div class="admin-news-content">
                                    По данным еженедельного доклада Управления по энергетической информации
                                    (EIA) при Минэнерго США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г.
                                    упал на 5,9 млн баррелей. По данным еженедельного доклада Управления по энергетической
                                    информации (EIA) при Минэнерго США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г.
                                    упал на 5,9 млн баррелей. По данным еженедельного доклада Управления по энергетической информации
                                    (EIA) при Минэнерго США (PDF), объем коммерческих
                                    США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                                    По данным еженедельного доклада Управления по энергетической информации
                                    (EIA) при Минэнерго США (PDF), объем коммерческих
                                    США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                                </div>
                                <div class="admin-news-date">
                                    23:55, 28 мая, 2015
                                </div>
                            </div>
                        </div>
                        <div class="admin-right-news">
                            <div class="admin-news-title">
                                <a href="#">Вы можете стать участником акции!</a>
                            </div>
                            <div class="admin-news-right-content">
                                По данным еженедельного доклада Управления по энергетической информации (EIA) при Минэнерго
                                США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                                По данным еженедельного доклада Управления по энергетической информации (EIA) при Минэнерго
                                США (PDF), объем коммерческих США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                                По данным еженедельного доклада Управления по энергетической информации (EIA) при Минэнерго
                                США (PDF), объем коммерческих
                                США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей. По данным еженедельного доклада
                                Управления по энергетической информации (EIA) при Минэнерго США (PDF), объем коммерческих
                                США за неделю до 18 декабря 2015 г. упал на 5,9 млн баррелей.
                            </div>
                            <div class="admin-news-date">
                                23:55, 28 мая, 2015
                            </div>
                        </div>
                        <div style="clear: both"></div>
                    </div>-->
                    <div class="my-news-div-style">
                        <div class="my-news-div-title">
                            Новости, которые вы опубликовали
                        </div>
                        <?php if($modelNews){ ?>
                            <?php foreach($modelNews as $news){ ?>
                                <div class="my-news-maine-div">
                                    <?php $img_src = '/images/default_avatar.jpg'; ?>
                                    <?php if(($news['img_src'] != '') && ($news['img_src'] != null)){ ?>
                                        <?php $img_src = '/'.$news['img_src']; ?>                                    
                                    <?php } ?>
                                    <div class="my-news-img" style="background-image: url(<?= $img_src; ?>)"></div>
                                    <div class="my-news-title">
                                        <div class="news-title">
                                            <a href="/newsshow/<?= $news['id']; ?>">
                                                <?= $news['title']; ?>
                                            </a>
                                        </div>
                                        <div class="news-date">
                                        </div>
                                    </div>
                                    <div class="my-news-content">
                                        <?php
                                            $content = strip_tags($news['content']);
                                            echo StringHelper::truncate($content, 60);
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <p style="text-align: center">Пока нет</p>
                        <?php } ?>
                    </div>
                    <div class="my-news-add">
                        <div class="news-add-title">
                            Разместить новость
                        </div>
                        
                        
                        
                        
                        <?php $form = ActiveForm::begin(); ?>
                            <span class="add-news-form-span">
                                Заголовок новости
                            </span>
                            <?= $form->field($modelNewNews, 'title')->textinput(['class' => 'add-news-input'])->label(false); ?>
                            <?= $form->field($modelNewNews, 'img_src')->hiddenInput()->label(false); ?>
                            <?= $form->field($modelNewNews, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false);; ?>
                            <div class="news-form-img-div">
                                <img src="/images/default_avatar.jpg" class="previewNewsImage" style="width:150px;height:150px;"><br>
                                <a class="btn btn-primary addNewsImage">Загрузить фото</a><br>
                            </div>
                            <?= $form->field($modelNewNews, 'content')->textarea(['class' => 'add-news-textarea'])->label(false); ?>
                                <?= Html::submitButton(Yii::t('app', 'Разместить'), ['class' => 'btn btn-primary']) ?>
                        <?php ActiveForm::end(); ?>  
                    </div>
                </div>
                
            </div>
        </section>
    </div>
</section>

<div style="display:none;">
    <div class="row-table-style">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">

            </div>
        </div>
    </div>
</div>