<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\LangWidget;
use app\models\Lang;
use app\assets\AppAsset;
AppAsset::register($this);

$langC = Lang::find()->where(['local' => Yii::$app->language])->asArray()->one();
$thisLangth = $langC['url'];

$arraySetting = \yii\helpers\ArrayHelper::map(\app\models\Setting::find()->all(), 'key', 'value');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div style="display:none;">
    <?php 
    NavBar::begin([]); 
    ?>
    <?php    
    NavBar::end();
    ?>
    </div>
    <nav class="top_line">
        <div class="wrapper">
            <div class="number_people_with_us_top_line">
                <ul>
                    <li>С нами уже <?= \app\models\User::find()->count(); ?> человек</li>
                    <li><a href="/rules_and_help"><span>?</span> Помощь по сайту</a></li>
                </ul>
            </div>
            <div class="user_office">
                <a href="#" class="your_question_top_line" onclick="openAskQuestionPage()">Задать вопрос онлайн</a>
                <div class="ask-question">
                    <span class="close-ask-question" ><img src="/img/close_ask_question.png" alt="" onclick="closeAskQuestion()"></span>
                    <div class="form-ask-question">
                        <h3>Задать вопрос онлайн</h3>
                        <input type="text" placeholder="Ваше имя" maxlength="255">
                        <input type="text" placeholder="Ваш е-mail" maxlength="255">
                        <textarea placeholder="Ваш вопрос" maxlength="255"></textarea>
                        <span class="publish">Отправить</span>
                    </div>
                </div>
                <ul class="personal_office">
                    <?php if(\Yii::$app->user->isGuest){ ?>
                        <li><a href="/login">Логин</a></li>                        
                        <li><span>/</span></li>
                        <li><a href="/signup">Регистрация</a></li>
                    <?php }else{ ?>
                        <li><a href="/profile">Личний кабинет</a></li>                        
                        <li><span>/</span></li>                        
                        <li>
                            <?= Html::beginForm(['/site/logout'], 'post'); ?>
                            <?=  Html::submitButton(
                                '<a>Logout (' . Yii::$app->user->identity->userinfo->username . ')</a>',
                                ['class' => 'btn-link']
                            ); ?>
                            <?= Html::endForm(); ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <header class="menu_header_top">
        <div class="wrapper">
            <div class="logo">
                <h2><a href="/">Логотип</a></h2>
            </div>
            <div class="menu_header_line">
                <ul>
                    <li><a href="#">Популярное <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#">Важное <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#">Организаторам</a></li>
                    <li><a href="#">Пользователям <i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#">Топ</a></li>
                    <li><a href="#">Связь с нами</a></li>
                </ul>
            </div>
            <div class="shopping_cart">
                <img src="/img/shopping_cart.png" alt="">
                <ul>
                    <li><a href="#">0шт.</a></li>
                    <li><span>/</span></li>
                    <li><a href="#">0 руб</a></li>
                </ul>
            </div>
            <div class="search_header">
                <input type="search" placeholder="Например: Мужские джинсы" />
                <input type="button" />
            </div>
        </div>
    </header>
    <section class="main_content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </section>

    <footer class="main_footer">
        <div class="col_1">
        </div>
        <div class="wrapper">
            <div class="col_2">
                
            </div>
            <div class="footer">
                <div class="social_menu_footer">
                    <p>Посетите нас в соцеальних сетях:</p>
                    <ul>
                        <li><a href="#" class="vk"></a></li>
                        <li><a href="#" class="mail"></a></li>
                        <li><a href="#" class="iam"></a></li>
                        <li><a href="#" class="pen"></a></li>
                        <li><a href="#" class="ok"></a></li>
                    </ul>
                    <span>Сделано в <a href="#">Рекламотерапии</a></span>
                </div>
                <div class="footer_nav_site_container">
                    <div class="important">
                        <h6>Важное</h6>
                        <ul>
                            <li><a href="#">Как заказать</a></li>
                            <li><a href="#">Как оплатить</a></li>
                            <li><a href="#">Как получить</a></li>
                        </ul>
                    </div>
                    <div class="people">
                        <h6>Пользователям</h6>
                        <ul>
                            <li><a href="#">Офисы раздач</a></li>
                            <li><a href="#">Новые закупки</a></li>
                            <li><a href="#">Товары в наличии</a></li>
                            <li><a href="#">Горящие покупки</a></li>
                        </ul>
                    </div>
                    <div class="popular">
                        <h6>Популярное</h6>
                        <ul>
                            <li><a href="#">Правила и помощь</a></li>
                            <li><a href="#">Статистика</a></li>
                            <li><a href="#">Форум</a></li>
                            <li><a href="#">Новости</a></li>
                        </ul> 
                    </div>
                    <div class="organizators_block">
                        <h6>Организаторам</h6>
                        <ul>
                            <li><a href="#">Как стать организатором</a></li>
                        </ul>  
                        <h6>Связь с нами</h6>
                        <ul>
                            <li><a href="#">Написать администрации сайта</a></li>
                        </ul>  
                    </div>
                    <div class="contact_footer">
                        <h2><?= isset($arraySetting['phone_number'])? $arraySetting['phone_number']:''; ?></h2>
                        <?php isset($arraySetting['email'])? $email = $arraySetting['email']: $email = ''; ?>
                        <a href="mailto:<?= $email; ?>"><?= $email; ?></a>  
                        <span>&copy;2015. Horom. Все права защищены</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
