<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
        <div class="about_us">
            <div class="wrapper">
                <ul class="about-us-description">
                    <li>
                        <a href="#">Xorom.ru</a>
                    </li>
                    <li>
                        <span><?= $countOrganizer; ?></span>
                        организаторов!
                    </li>
                    <li>
                        <span><?= $countProduct; ?></span>
                        товаров!
                    </li>
                    <li>
                        <span><?= $countZacup; ?></span>
                        закупок!
                    </li>
                    <li>
                        <span><?= $countUser; ?></span>
                        пользователей!
                    </li>
                </ul>
                <div class="content_main_page">
                    <div class="home-welcome">
                        <img src="/img/home_welcome_background.png" alt="">
                        <div class="description-home-welcome">
                            <h2>Добро пожаловать на сайт совместных покупок!</h2>
                            <p>Наконец-то стало удобно совершать совместные покупки. Наш сервис с невероятно простым и удобным интерфейсом поможет вам легко совершать совместные покупки!</p>
                            <ul class="send_message_my_friends">
                                <li><a href="/signup" class="publish">Простая регистрация!</a></li>
                                <li><a href="#">Воспользуйтесь всеми преимуществами нашего сервиса</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="category">
                        <!--<div class="content_category">
                            <span class="category_added">+45</span>
                            <img src="/img/categry_img_2.png" alt="">
                            <h6>Для женщин</h6>
                        </div>-->
                        <?php foreach($modeCategory as $category){ ?>
                        <a href="/new/category/<?= $category['id']; ?>">
                            <div class="content_category">
                                <?php $image_src = ''; ?>
                                <?php if(($category['img_src'] != '') && ($category['img_src'] != null)){ ?>
                                    <?php $image_src = $category['img_src']; ?>
                                <?php } ?>
                                <img src="<?= $image_src; ?>" alt="">
                                <h6><?= $category['name']; ?></h6>
                            </div>
                        </a>
                        <?php } ?>
                        
                    </div>
                    <div class="advertising_banners">
                        <div class="advertising_banners_content">
                            <h4>Рекламный баннер</h4>
                            <p>Приглашаем всех на открытие нового магазина игрушек</p>
                        </div>
                        <div class="advertising_banners_content">
                            <h4>Рекламный баннер</h4>
                            <p>Приглашаем всех на открытие нового магазина игрушек</p>
                        </div>
                        
                    </div>
                    <div class="block_news">
                            <div class="latest_news">
                                <h2>Свежие новости</h2>
                                <ul>
                                    <li>
                                        <p>Сегодня мы добавили 50 новых товаров!</p>
                                        <span class="content_news"> Чтобы увидеть новые товары вы можете зайти в раздел Внимание новые закупки!”</span>
                                        <span class="date_write">23.55, 28 мая, 2015</span>
                                    </li>
                                    <li>
                                        <p>Сегодня мы добавили 50 новых товаров!</p>
                                        <span class="content_news"> Чтобы увидеть новые товары вы можете зайти в раздел Внимание новые закупки!”</span>
                                        <span class="date_write">23.55, 28 мая, 2015</span>
                                    </li>
                                </ul>
                                <button class="all_news">Все новости</button>
                            </div>
                            <div class="top_organizers">
                                <h2>ТОП организаторов</h2>
                                <ul>
                                    <li>
                                        <p>Магазин детской одежды “Малыш”</p>
                                        <span class="scores ">Всего баллов 243</span>
                                    </li>
                                    <li>
                                        <p>Организатор совместных покупок Светлана</p>
                                        <span class="scores">Всего баллов 243</span>
                                    </li>
                                    <li>
                                        <p>Организатор покупок Виктор Комаров</p>
                                        <span class="scores">Всего баллов 243</span>
                                    </li>
                                    <li>
                                        <p>Организатор покупок Виктор Комаров</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                </div>
                <div class="hot_news">
                    <img class="video" src="img/video.png" alt="">
                    <ul>
                        <li>
                            <a href="/hot/category">
                                <img src="img/fire.png" alt="">
                                <h5>Горящие закупки!</h5>
                                <p>Успейте заказать! Возможно мы ждем именно вас!</p>
                            </a>
                        </li>
                        <li>
                            <a href="/product/category">
                                <img src="img/pack.png" alt="">
                                <h5>Товары в наличии</h5>
                                <p>Сумка женская, кожа и ткань. Приемлемые цены, отличное качество!</p>
                            </a>
                        </li>
                        <li>
                                <img src="img/icon_attention_new.png" alt="">
                                <div class="content_attention_new">
                                    <h5>Собираем заказы!<br/> Новые закупки</h5>
                                    <p>
                                        Посмотрите новые закупки на которые организаторы еще, только, собирают заказы. Все что угодно в общем.
                                    </p>
                                    <span class="publish">
                                        <a href="/new/category">
                                            Посмотреть
                                        </a>
                                    </span>
                                </div>
                        </li>
                    </ul>
                    <div class="advertising_banners_content">
                            <h4>Рекламный баннер</h4>
                            <p>Приглашаем всех на открытие нового магазина игрушек</p>
                    </div>
                    <div class="best_organizer">
                        <span class="date_write">23.55, 28 мая, 2015</span>   
                        <h2>Лучший организатор!</h2>
                        <div class="best_user_organize">
                            <img src="img/avatar_user_best.png" alt="">
                            <div class="advertisement_best_user">
                                <p>
                                    По итогам продаж прошлой недели мы торопимся сообщить о новом лучшем “продавце недели”.  Им становится магазин “Мужская обувь”! Который завоевал целых 590 баллов и оказался на верхушке рейтинга, уделав всех остальных в два счета! Поздравляем!
                                </p>
                                <span class="publish">Все объявления этого организатора</span>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
