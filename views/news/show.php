<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\ProductAsset;
    ProductAsset::register($this);
    
$this->title = 'My Yii Application';
?>

    <section class="menu_content">
        <div class="wrapper">
            <div class="navigation_blo22">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Новости / 2015 Сентябр</a></li>
                    <li class="li_cls_col"><a href="#"><?= $modelNews['title']; ?></a></li>
                </ul>
            </div>
            <div class="new_22">
                <div class="news_title2">
                    <h1><span><?= $modelNews['title']; ?></span></h1>
                </div>
            </div>
        </div>
    </section>
    <section class="main_content">
        <div class="wrapper">
            <section class="user_profile22">
                <div class="my_selected22">
                    <div class="whit_ul_clas">
                        <div class="pos_ab">
                            <a href="#">
                                <div class="data_com222"><?= $modelNews['date_create']; ?></div>
                                <!--<span class="arrow-news-left"><img src="/img/arrow_news-left.png" alt=""></span>-->
                                <?php $img_src = '/images/default_avatar.jpg'; ?>
                                <?php if(($modelNews['img_src'] != '') && ($modelNews['img_src'] != null)){ ?>
                                    <?php $img_src = '/'.$modelNews['img_src']; ?>
                                <?php } ?>
                                <div class="whit_blok">
                                    <img class="pad_img" src="<?= $img_src; ?>" alt="">
                                </div>
                                <div class="title_com">
                                    <p><?= $modelNews['content']; ?></p>
                                    <a href="/news" class="bot_p">Вернуться к разделу Новости</a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>