<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\ProductAsset;
    ProductAsset::register($this);
    use yii\helpers\StringHelper;
$this->title = 'My Yii Application';
?>

    <section class="menu_content">
        <div class="wrapper">
            <div class="navigation_block">
                <ul>
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Новости / 2015 Сентябр</a></li>
                </ul>
            </div>
            <div class="new_22">
                <div class="news_title2">
                    <h1><span>Новости </span>/ <span>2015</span>/ <span>Сентябрь</span></h1>
                </div>
                <div class="news_title33">
                    <div class="month_arxiv">
                        <span>Архив новостей:</span>
                    </div>
                    <div class="month-select-div-style2">
                        <select class="month-select-style2">
                            <option>2015</option>
                            <option>2016</option>
                            <option>2017</option>
                        </select>
                    </div>
                    <div class="month-select-div-style22">
                        <select class="month-select-style22">
                            <option>Сентябрь</option>
                            <option>Апрель</option>
                            <option>Март</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main_content">
        <div class="wrapper">
            <section class="user_profile22">
                <div class="my_selected22">
                    <?php foreach($modelNews as $news){ ?>
                        <ul class="whit_ul_clas">
                            <li>
                                <a href="#">
                                    <div class="whit_blok"><img class="pad_img" src="img/news_foto.png" alt=""></div>
                                    <div class="title_com"><?= $news['title']; ?></div>
                                    <div class="disk_com">
                                         <?php
                                            $content = strip_tags($news['content']);
                                            echo StringHelper::truncate($content, 60);
                                        ?>
                                    </div>
                                    <div class="data_com">21 декабря, 2015</div>
                                </a>
                            </li>
                        </ul>
                    <?php } ?>
                    
                </div>
            </section>
        </div>
    </section>