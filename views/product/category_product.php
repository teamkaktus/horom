<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\ProductAsset;
    ProductAsset::register($this);
    
$this->title = 'My Yii Application';
?>
<section class="menu_content">
    <div class="wrapper">
        <div class="navigation_block">
            <ul>
                <li><a href="home_page.html">Главная</a></li>
                <li><a href="#">Товари в в наличии</a></li>
            </ul>
        </div>
    </div>
</section>
<section class="main_content_product_in_stock">
    <div class="wrapper">
        <div class="conteinar_title_product_in_stock">
            <span class="title_product_in_stock">Товары в наличии</span>
            <span class="title_text_product_in_stock">Всего <?= $stringProductCount; ?> товаров в наличии</span>
            <span class="title_text_product_in_stock2">Смотреть также:</span>
        </div>
        <div class="conteinar_title_product_in_stock2">
            <div class="product_in_stock_block">
                <a href="/new/category">
                    <span class="title_text_block_product_in_stock">Новые закупки</span>
                </a>
            </div>
            <div class="product_in_stock_block2">
                <a href="/hot/category">
                    <span class="title_text_block_product_in_stock">Горящие закупки</span>
                </a>
            </div>
        </div>
        <div class="clear-both"></div>
    </div>
</section>
<section class="main_content_product_in_stock">
    <div class="wrapper">
        <div class="conteinar_burning-purchases_filter_left ">
            <span class="title_filter_left">Фильтр по разделам:</span>
            <div id="menu_body">
                <ul class="conteinar_filter_style_burning-purchases">
                    
                    <?php foreach($modelCategories as $category){ ?>
                            <li class="filter_name_style_burning-purchases">
                                <div onclick="openMenu('sub_menu_2',this);return(false)" class="div_menu_style">
                                    <?= $category['name']; ?><span class="filter_digit_style">(1300)</span></div>
                                <ul id="sub_menu_2" class="sub_menu">
                                    <li><a href="#">Сумки</a></li>
                                </ul>
                            </li>                        
                    <?php } ?>
                            
                </ul>
            </div>
        </div>
        <div class="product_conteinar_style">
            <span class="product_text_style_product_in_stock">Здесь находятся товары, которые есть в наличии на данный момент</span>
            <div class="border_conteinar_style"></div>
                    <div class="filter_menu">
                        <div class="filter_block1">
                            <span class="filter_text_style">Размер изделия:</span>
                            <div class="padding_filter">
                                <button class="fixing_size1"><span class="fixing_size">XS</span></button>
                                <button class="fixing_size2"><span class="fixing_size">S</span></button>
                                <button class="fixing_size3"><span class="fixing_size">M</span></button>
                                <button class="fixing_size4"><span class="fixing_size">L</span></button>
                                <button class="fixing_size5 active"><span class="fixing_size">XL</span></button>
                                <button class="fixing_size6"><span class="fixing_size">XXL</span></button>
                                <button class="fixing_size7"><span class="fixing_size">XXXL</span></button>
                            </div>
                        </div>
                        <div class="filter_block2">
                            <span class="filter_text_style">Цвет:</span>
                            <div class="padding_filter_color icon_right_style1">
                                <div id="div-color-style" class="div-color-style"></div>
                                <select id="mounth">
                                    <option value="#4e8518" rel="icon-temperature" style="background-color: #4e8518;"></option>
                                    <option value="#181b85" style="background-color: #181b85;"></option>
                                    <option value="#851881" style="background-color: #851881;"</option>
                                    <option value="#00fcff" style="background-color: #00fcff;"></option>
                                </select>
                            </div>
                        </div>
                        <div class="filter_block3">
                            <span class="filter_text_style">Популярность:</span>
                            <div class="padding_filter_color icon_right_style">
                                <select class="popularity_filter">
                                    <option>Популярные</option>
                                    <option>Новинки</option>
                                </select>
                            </div>
                        </div>
                        <div class="border_filter_left"></div>
                        <div class="filter_block4">
                            <span class="filter_text_style">Цена:</span>
                            <div class="padding_filter_color">
                                <input class="container_filter_price1" type="text" placeholder="От">
                                <input class="container_filter_price2" type="text" placeholder="До">
                                <span class="filter_price_text_style">Руб.</span>
                            </div>
                        </div>
                    </div>
            <div class="row">
                <?php foreach($modelProduct as $product){ ?> 
                    <div class="container_tovar" style="float:none;">
                        <div class="box_shadow_container">
                            <a href="/product/<?= $product->product->id; ?>">
                                <?php if(($product->product->img_src != '') && ($product->product->img_src != null)){ ?>
                                <img class="image_product_in_stock" src="<?= $product->product->img_src; ?>">
                                <?php }else{ ?>
                                    <img class="image_product_in_stock" src="/images/default_avatar.jpg">
                                <?php } ?>
                            </a>

                            <?= app\widgets\ChechfavoriteWidget::widget(['product_id' => $product->product->id]); ?>
                            <div class="name_container_product_in_stock">
                                <a href="/product/<?= $product->product->id; ?>">
                                    <span class="name_product_in_stock"><?= $product->product->name; ?></span>
                                </a>
                            </div>
                            <?= app\widgets\UseroncatalogWidget::widget(['user_id' => $product->product->user_id, 'type' => 'tovar']); ?>
                            <div class="data_and_price_container">
                                <div class="data_container_product_in_stock">
                                    <span class="data_text_product_in_stock"><?= $product->product->date_create; ?></span>
                                </div>
                                <div class="price_container_product_in_stock">
                                    <span class="price_text_product_in_stock"><?= $product->product->price; ?> руб</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?> 
            </div>
            
            
        </div>
        
    </div>
    <div class="clear-both"></div>
</section>