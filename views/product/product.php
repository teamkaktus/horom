<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\ProductAsset;
    ProductAsset::register($this);
    
$this->title = 'My Yii Application';
?>
<section class="main_content">
    <div class="wrapper">
        <div class="title-cards-goods">
            <h1><?= $modelProduct->name; ?></h1>
            <div class="come-back">
                <span class="publish"><a href="">Назад</a></span>
            </div>
            <div class="info-user-card-goods">
                <?php if(($modelProduct->user->userinfo->avatar != null) && ($modelProduct->user->userinfo->avatar != '')){ ?>  
                    <img src="/<?= $modelProduct->user->userinfo->avatar; ?>" style="width:60px;" alt="">
                <?php }else{ ?>  
                    <img src="/images/default_avatar.jpg" style="width:60px;" alt="">
                <?php } ?>
                    
                <h3><?= $modelProduct->user->userinfo->username; ?></h3>
                <i>
                    <b>
                        <?php 
                            switch ($modelProduct->user->type) {
                                case 'сompetitor':
                                    echo "Соескатель";
                                    break;
                                case 'organizer':
                                    echo "Организатор";
                                    break;
                                case 'admin':
                                    echo "Администратор";
                                    break;

                                default:
                                    break;
                            }
                        ?>
                    </b>
                </i>
                <a href="#">Отправить сообщение</a>
            </div>
        </div>
        <div class="cards-goods-content">
          <div class="title-goods" style="height:auto;">
            <?php if(($modelProduct->img_src != '') && ($modelProduct->img_src != null)){ ?>
                <div class="owl-carousel owl-theme owl-carousel_style_burning-purchases" style="width:100%;">
                    <div class="item" style="background-image: url('/images/product/<?= $modelProduct->img_src; ?>');height:175px;width:100%"></div>
                    <?php if(count($modelProduct->images) > 0){ ?>
                        <?php foreach($modelProduct->images as $image){ ?>
                            <div class="item" style="background-image: url('/images/product/<?= $image->img_src; ?>');height:175px;width:100%"></div>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <p style="margin-top:40px;">
                <?= $modelProduct->about; ?>
            </p>
          </div>
          <div class="block-add-card">
            <span class="user_rating">Рейтинг организатора - <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> 251</a></span>
            <span class="price-good"><?= $modelProduct->price; ?> <span>руб</span></span>
            <strong>Информация о доставке: <a href="#">Подробнее о доставке</a></strong>
            <div class="information-block-card">
              <p>Доставка + 200 - 350 р.</p>
              <p>Доставка сразу на ваш адрес 100 % оплата заказа, Сбербанк России.</p>
              <p>Оценка посылки по требованию</p>
            </div>
            <strong>Как получить товар: <a href="#">Подробнее о получении</a></strong>
              <div class="information-block-card">
                <p>1 класс (почта России)</p>
                <p>Самовывоз</p>
              </div>
            <strong>Параметры заказа:</strong>
            <p>
              Размер изделия:
              <select>
                <option>XXXl</option>
              </select>
            </p>
            <p>
              <div class="number" data-hr-counter="">
              </div>
            </p>
            <p class="delivery">
              Доставка:
              <select>
                <option>Самовивоз</option>
              </select>
            </p>
            <span class="button">Добавить в корзину</button>
          </div>
        </div>
        <h1 class="comment-header-description">Вопросы и отзывы:</h1>
        <div class="comment-section">
            <div class="comment-simplebox">
                <?php if(!\Yii::$app->user->isGuest){ ?>
                
                 <?php
                    if(Yii::$app->session->hasFlash('comment_added')):
                        echo Alert::widget([
                                'options' => [
                                        'class' => 'alert-info',
                                ],
                                'body' => 'Отзив добавлено',
                        ]);
                    endif;
                    ?>
                     <?php $form = ActiveForm::begin(); ?>
                        <?php if((\Yii::$app->user->identity->userinfo->avatar != null) && (\Yii::$app->user->identity->userinfo->avatar != '')){ ?>  
                            <img src="/<?= \Yii::$app->user->identity->userinfo->avatar; ?>" style="width:80px;" alt="">
                        <?php }else{ ?>
                            <img src="/images/default_avatar.jpg"  style="width:80px;" alt="">
                        <?php } ?>
                        <?= $form->field($modelNewComments, 'product_id')->hiddenInput(['value' => $modelProduct->id])->label(false); ?>
                        <?= $form->field($modelNewComments, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                        <?= $form->field($modelNewComments, 'description')->textarea(['placeholder' => 'Задайте ваши вопросы или оставьте отзыв об организаторе'])->label(false); ?>
                        <?= $form->field($modelNewComments, 'image_array')->hiddenInput()->label(false); ?>

                        <div id="previewsCommentFotoContainer" class="row"></div>
                        <div class="block-button">
                            <?= Html::submitButton(Yii::t('app', 'Опубликовать'), ['class' => 'publish']) ?>
                            <span class="attach-photo addCommentPhoto">Прикрепить фото</span>
                        </div>
                    <?php ActiveForm::end(); ?> 
                <?php }else{ ?>
                            <p style="text-align: center">Чтобы добавить отзыв нужно авторизоваться</p>
                <?php } ?>
            </div>
          <div class="comment-cards-goods">
            
                <?php function showComment($comment){ 
                        $useravatar = '<img src="/images/default_avatar.jpg" style="width:60px;" alt="">';
                        if (($comment->user->userinfo->avatar != null) && ($comment->user->userinfo->avatar != '')) { 
                            $useravatar = '<img src="/'.$comment->user->userinfo->avatar.'" style="width:60px;" alt="">';
                        } 
                        $commentImages = '';
                            if(($comment->img_src != null) && ($comment->img_src != '')){
                            $commentImages .= '<img src="/images/comment/'.$comment->img_src.'" style="width:60px;max-height: 70px;float: left" alt="">';

                                if($comment->images){ 
                                    foreach($comment->images as $c_image){
                                        $commentImages .= '<img src="/images/comment/'.$c_image->img_src.'" style="width:60px;" alt="">';
                                    }
                                }
                            }
                            
                            $childComment = '';
                            if($comment->childcomment > 0){
                                foreach ($comment->childcomment as $comment_c) {
                                    $childComment .= '<div class="well">'.showComment($comment_c).'</div>';
                                    //var_dump($childComment);
                                }
                            }
                
                    return '<div class="row">'.
                                '<div class="col-sm-2">'.
                                    $useravatar.
                                '</div>'.
                                '<div class="col-sm-10">'.
                                    '<div class="content-comment-cards-goods">'.
                                        '<a href="#" class="user_name">'.$comment->user->userinfo->username.'</a>'.
                                        '<div class="date_write">'.
                                            '<span>'.$comment->date_create.'</span>'.
                                        '</div>'.
                                        '<p>'.$comment->description.'</p>'.
                                        '<div class="row">'.
                                            $commentImages.
                                        '</div>'.
                                        '<a class="answerToComment" data-comment_id="'.$comment->id.'">Ответить</a>'.
                                            $childComment.
                                    '</div>'.
                                '</div>'.
                            '</div>';
                } ?>
                <?php foreach ($modelComment as $comment) { ?>
                    <?= showComment($comment); ?>
                <?php } ?>
                
            <div id="scrollup"><img alt="Прокрутить вверх" src="/img/up.png"></div>
          </div>
        </div>
    </div>
</section>


<div style="display:none;">
    <div class="table table-striped" class="files" id="previews">

        <div id="template" class="file-row">
          <!-- This is used as the file preview template -->
          <div class="col-sm-1">
                  <a data-dz-remove class="btn btn-danger delete" style="position:absolute;top:0px;right:10px;cursor:pointer;">
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                <div>
                    <span class="preview"><img data-dz-thumbnail style="width:100%;max-height: 100px"/></span>
                </div>
                <div>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                      <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </div>
            </div>
        </div>

      </div>
</div>
<div style="display:none;">
    <div class="table table-striped" class="files" id="previews">

        <div id="template_a" class="file-row">
          <!-- This is used as the file preview template -->
          <div class="col-sm-3">
                  <a data-dz-remove class="btn btn-danger delete" style="position:absolute;top:0px;right:10px;cursor:pointer;">
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                <div>
                    <span class="preview"><img data-dz-thumbnail style="width:100%;max-height: 100px"/></span>
                </div>
                <div>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                      <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </div>
            </div>
        </div>

      </div>
</div>
<?php if(!\Yii::$app->user->isGuest){ ?>
<div id="addNewCommentAnswer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Ответить на отзыв</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(); ?>
                    <?php if((\Yii::$app->user->identity->userinfo->avatar != null) && (\Yii::$app->user->identity->userinfo->avatar != '')){ ?>  
                        <img src="/<?= \Yii::$app->user->identity->userinfo->avatar; ?>" style="width:80px;" alt="">
                    <?php }else{ ?>
                        <img src="/images/default_avatar.jpg"  style="width:80px;" alt="">
                    <?php } ?>
                    <?= $form->field($modelNewComments, 'product_id')->hiddenInput(['value' => $modelProduct->id])->label(false); ?>
                    <?= $form->field($modelNewComments, 'parent_id')->hiddenInput()->label(false); ?>
                    <?= $form->field($modelNewComments, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                    <?= $form->field($modelNewComments, 'description')->textarea(['placeholder' => 'Задайте ваши вопросы или оставьте отзыв об организаторе'])->label(false); ?>
                    <?= $form->field($modelNewComments, 'image_array')->hiddenInput()->label(false); ?>

                    <div id="previewsAnswerFotoContainer" class="row"></div>
                    <div class="block-button">
                        <?= Html::submitButton(Yii::t('app', 'Опубликовать'), ['class' => 'publish']) ?>
                        <span class="attach-photo addAnswerPhoto">Прикрепить фото</span>
                    </div>
                <?php ActiveForm::end(); ?> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>