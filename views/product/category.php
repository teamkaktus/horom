<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\ProductAsset;
    ProductAsset::register($this);
    
$this->title = 'My Yii Application';
?>
<section class="menu_content">
    <div class="wrapper">
        <div class="navigation_block">
            <ul>
                <li><a href="home_page.html">Главная</a></li>
                <li>
                    <?php if($type != null){ ?>
                        <?php if($type == 'hot'){ ?>
                            <a href="#">Горящие закупки</a>
                        <?php }elseif($type == 'new'){ ?>
                            <a href="#">Новые закупки</a>
                        <?php } ?>
                    <?php } ?>
                    
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="main_content_product_in_stock">
    <div class="wrapper">
        <div class="conteinar_title_product_in_stock">
            <span class="title__burning-purchases">
                <?php if($type != null){ ?>
                    <?php if($type == 'hot'){ ?>
                        Горящие закупки
                    <?php }elseif($type == 'new'){ ?>
                        Новые закупки
                    <?php } ?>
                <?php } ?>
            </span>
            <span class="title_text_product_in_stock">Всего <?= $stringProductCount; ?> 
                <?php if($type != null){ ?>
                    <?php if($type == 'hot'){ ?>
                        горящих
                    <?php }elseif($type == 'new'){ ?>
                        Новых
                    <?php } ?>
                <?php } ?>                
                закупок</span>
            <span class="title_text_product_in_stock2">Смотреть также:</span>
        </div>
        <div class="conteinar_title_product_in_stock2">
            <div class="product_in_stock_block">
                <?php if($type != null){ ?>
                    <?php if($type == 'hot'){ ?>
                        <a href="/new/category">
                            <span class="title_text_block_product_in_stock">Новые закупки</span>
                        </a>
                    <?php }elseif($type == 'new'){ ?>
                        <a href="/hot/category">
                            <span class="title_text_block_product_in_stock">Горящие закупки</span>
                        </a>
                    <?php } ?>
                <?php } ?>
                
            </div>
            <div class="product_in_stock_block2">
                <a href="/product/category">
                    <span class="title_text_block_product_in_stock">Товары в наличии</span>                    
                </a>
            </div>
        </div>
        <div class="clear-both"></div>
    </div>
</section>
<section class="main_content_product_in_stock">

    <div class="wrapper">
        <div class="conteinar_burning-purchases_filter_left ">
            <span class="title_filter_left">Фильтр по разделам:</span>
            <div id="menu_body">
                <ul class="conteinar_filter_style_burning-purchases">
                    
                    <?php foreach($modelCategories as $category){ ?>
                            <li class="filter_name_style_burning-purchases">
                                <div onclick="openMenu('sub_menu_2',this);return(false)" class="div_menu_style">
                                    <?= $category['name']; ?><span class="filter_digit_style">(1300)</span></div>
                                <ul id="sub_menu_2" class="sub_menu">
                                    <li><a href="#">Сумки</a></li>
                                </ul>
                            </li>                        
                    <?php } ?>
                            
                </ul>
            </div>
        </div>
        <div class="burning-purchases_conteinar_top_style">
            <div class="burning-purchases_conteinar1">
                <span class="filter_text_style">Размер изделия:</span>
                <div class="padding_buttons_burning-purchases_conteinar1">
                    <button class="button_burning-purchases_conteinar1"><span class="fixing_size_burning-purchases">Закупками</span>
                    </button>
                    <div class="border_burning-purchases_right"></div>
                    <button class="button2_burning-purchases_conteinar1"><span class="fixing_size_burning-purchases">Товарами</span>
                    </button>
                </div>
            </div>
            <div class="burning-purchases_conteinar2">
                <div class="container_burning-purchases2">
                    <span class="filter_text_style">Укажите статус сбора заказа:</span>
                    <div class="padding_buttons_burning-purchases_conteinar1 icon_right_style2">
                        <select class="status_filter_burning-purchases text_status_burning-purchases">
                            <option class="text_status_burning-purchases">Сбор заказов окончен</option>
                            <option>Сбор заказов открит</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <?php foreach($modelProduct as $product){ ?>        
            <div class="contaeiner_tavar_burning-purchases">
                <div class="container_name_tovar_burning-purchases">
                    <span class="name_tovar_burning-purchases">
                        <a href="/product/<?= $product->product->id; ?>"><?= $product->product->name; ?></a>
                    </span></br>
                    <span class="data_text_product_in_stock"><?= $product->product->date_create?></span>
                </div>
                <div class="container_name_tovar_burning-purchases2">
                    <?= app\widgets\UseroncatalogWidget::widget(['user_id' => $product->product->user_id, 'type' => 'zacup']); ?>
                </div>
                <div class="border_burning-purchases"></div>
                <div class="name_container_tovar_burning-purchases">
                    <span class="name_tovar_burning-purchases_style">
                        <a href="/product/<?= $product->product->id; ?>"><?= $product->product->short_description; ?></a>
                    </span>
                    <span class="price_tovar_burning-purchases_style"><?= $product->product->price; ?> руб</span>
                    <?php if(($product->product->img_src != '') && ($product->product->img_src != null)){ ?>
                        <div class="owl-carousel owl-theme owl-carousel_style_burning-purchases">
                            <div class="item" style="background-image: url('/images/product/<?= $product->product->img_src; ?>');height:175px;"></div>
                            <?php if(count($product->productImages) > 0){ ?>
                                <?php foreach($product->productImages as $image){ ?>
                                    <div class="item" style="background-image: url('/images/product/<?= $image->img_src; ?>');height:175px;"></div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="text_bottom_image">
                    <span class="text_bottom_image_style">До заполнения осталось:</span>
                    <div class="border_burning-purchases_style">
                        <div class="style_text_in_border_burning-purchases_style">64%</div>
                    </div>
                    <span class="text_bottom_image_style2"> / 100%</span>
                    <span class="day_text_style_burning-purchases">1 день</span>
                    <div>
                        <a href="" class="padding_img_soc_2"></a>
                        <a href="" class="padding_img_soc_3"></a>
                        <a href="" class="padding_img_soc_4"></a>
                        <a href="" class="padding_img_soc_5"></a>
                        <a href="" class="padding_img_soc_1"></a>
                    </div>
                </div>
            </div>
        <?php } ?>
        
        

    </div>
    <div class="clear-both"></div>

</section>
<?php // var_dump($modelCategories); ?>
<?php // var_dump($modelProduct); ?>