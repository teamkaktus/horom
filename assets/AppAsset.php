<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/design/become_an_organizer.css',
        'css/OwlCarousel2-2.2.1/owl.carousel.min.css',
        'css/OwlCarousel2-2.2.1/owl.theme.default.min.css',
        'css/design/burning-purchases.css',
        'css/design/cards-goods.css',
        'css/design/comments_and_responses.css',
        'css/design/fonts.css',
        'css/design/home_page.css',
        'css/design/how_order.css',
        'css/design/main.css',
        'css/design/message.css',
        'css/design/my_accounts.css',
        'css/design/my_friends.css',
        'css/design/my_orders.css',
        'css/design/news.css',
        'css/design/news_my.css',
        'css/design/orders_my.css',
        'css/design/page_news.css',
        'css/design/page_one_news.css',
        'css/design/personal-office-1.css',
        'css/design/product_in_stock.css',
        'css/design/registration.css',
        'css/design/reviews.css',
        'css/design/rules_and_help.css',
        'css/design/selected.css',
        'css/design/stylesheet_style.css',
        'css/design/vars.css',
        'css/sweetalert.css',
        'css/site.css',
    ];
    public $js = [
        'js/plugins/OwlCarousel2-2.2.1/owl.carousel.min.js',
        'js/plugins/sweetalert.min.js',
        'js/dropzone.js',
        'js/common.js',
        'js/cabinet.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
