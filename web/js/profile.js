
$(document).ready(function(){
    
    
    if($('.changeProfilePhoto').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            url: "../../../profile/default/saveprofilephoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".changeProfilePhoto",
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.userProfileImage').attr('src', '/'+response.xhr.response)
                $('[name=image_prodile_src]').val(response.xhr.response);
                $('.blockSavePhotoButton').show();
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
    
    $(document).on('click', '.saveProfilePhoto',function(){
        var image_src = $('[name=image_prodile_src]').val();
        $.ajax({
            type: 'POST',
            url: '../../../../profile/default/updateprofilephoto',
            data: {image_src:image_src},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    swal("Аватар сохранено!", "", "success")
                    $('.blockSavePhotoButton').hide();
                }
            }
        });            
    })
    
    
});

