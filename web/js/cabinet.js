$(document).ready(function () {
    
    if($('.addNewsImage').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var newsDropzone = new Dropzone($('.addNewsImage')[0], {
            url: "../../../profile/news/savenewsphoto",
            previewTemplate: previewTemplate,
            previewsContainer: "#previews",
            clickable: ".addNewsImage",
            uploadMultiple:false,
            maxFiles:1,
        });

        newsDropzone.on("maxfilesexceeded", function(file) {
                newsDropzone.removeAllFiles();
                newsDropzone.addFile(file);
        });

        newsDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.previewNewsImage').attr('src', '/'+response.xhr.response)
                $('#news-img_src').val(response.xhr.response);
            }
        });

        newsDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });

    }
    
    
    $(document).on('change','#product-category_id',function(){
        var category_id = $(this).val();
        $.ajax({
            method:'post',
            url:'/site/getsubcategory',
            data:{category_id:category_id},
            dataType:'json',            
        }).done(function(response){
            $('#product-sub_category').html('');
            console.log(response.length);
            if(response.length > 0){
                $('#block-sub_category').css('display','block');
                for(var key in response){
                    var category = response[key];
                    $('#product-sub_category').append('<option value="'+category['id']+'">'+category['name']+'</option>');
                }
            }else{
                $('#block-sub_category').css('display','none');                
            }
            console.log(response)
        })
    })
    
    if($('.addPhoto').length){
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var photosArray = [];
            if($('#product-image_array').val() != ''){
                photosArray = $.parseJSON($('#product-image_array').val());              
            }
            console.log(photosArray);
            var myDropzoneG = new Dropzone($('.addPhoto')[0], {
                url: "../../../profile/product/saveproductimg",
                maxFiles:10,
                previewTemplate: previewTemplate,
                previewsContainer: "#previewsFotoContainer", 
                clickable: ".addPhoto" ,
            });

            myDropzoneG.on("maxfilesexceeded", function(file) {
            });

            myDropzoneG.on("complete", function(response) {
                if (response.status == 'success') {
                    
                    photosArray.push(response.xhr.response);
                    $('#product-image_array').val(JSON.stringify(photosArray));
                    
                }else{
                    //$('.maxPhoto').show()
                }
            });

            myDropzoneG.on("removedfile", function(response) {
                if(response.xhr != null){
                    var removeItem = response.xhr.response;
                    photosArray = jQuery.grep(photosArray, function(value) {
                        return value != removeItem;
                    });
                    $('#product-image_array').val(JSON.stringify(photosArray));
                }
            });
            
            $(document).on('click','.deleteProductImage',function(){
                var shos = $(this).parent();
                var name_photo = $(this).data('name');
                console.log(name_photo);
                $.each( myDropzoneG.files, function( key, value ) {
                    if(value.xhr.response == name_photo){
                        myDropzoneG.removeFile(myDropzoneG.files[key]);
                    }
                });
                $( ".addPhoto" ).each(function() {
                    var thisBackground = $(this).css('background-image');
                    if ( thisBackground.indexOf(name_photo) !== -1 ){
                            delete(photosArray[name_photo]);
                            var removeItem = name_photo;
                            photosArray = jQuery.grep(photosArray, function(value) {
                                return value != removeItem;
                              });
                        $('#product-image_array').val(JSON.stringify(photosArray));

                        $(this).css('background-image', 'url(/images/default_avatar.jpg)')
                        $(this).parent().find('.deleteProductImage').hide();
                        $(this).data('delete_status','no')
                    }
                })
                
            });
    }
    
    
    $( ".NewProductImageBlock" ).hover(
        function() {
            if($(this).data('delete_status') == 'yes'){                
                $(this).parent().find('.deleteProductImage').show();
            }
            $(this).css('box-shadow', 'none');
        }, function() {
            var classH = $(this).parent().find('.deleteProductImage');
            if (classH.is(':hover')) {
            } else {
                classH.hide();
                classH.parent().find('.NewProductImageBlock').css('box-shadow', '0 0 5px rgba(0,0,0,0.5)');
            }
        }
    );
    
    

});


