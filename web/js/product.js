$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText: ['<img src="/img/strilka_left.png" />', '<img src="/img/strilka_right.png" />'],
        items:1,
        dots: false
    })
    
    
    if($('.addCommentPhoto').length){
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var photosArray = [];

            var myDropzoneG = new Dropzone($('.addCommentPhoto')[0], {
                url: "/product/savecommentimg",
                maxFiles:10,
                previewTemplate: previewTemplate,
                previewsContainer: "#previewsCommentFotoContainer", 
                clickable: ".addCommentPhoto" ,
            });

            myDropzoneG.on("maxfilesexceeded", function(file) {
            });

            myDropzoneG.on("complete", function(response) {
                if (response.status == 'success') {
                    
                    photosArray.push(response.xhr.response);
                    $('#productcomment-image_array').val(JSON.stringify(photosArray));
                    
                }else{
                    //$('.maxPhoto').show()
                }
            });

            myDropzoneG.on("removedfile", function(response) {
                if(response.xhr != null){
                    var removeItem = response.xhr.response;
                    photosArray = jQuery.grep(photosArray, function(value) {
                        return value != removeItem;
                    });
                    $('#productcomment-image_array').val(JSON.stringify(photosArray));
                }
            });
    }
    
    if($('.addAnswerPhoto').length){
            var previewNode = document.querySelector("#template_a");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var photosArray = [];

            var myDropzoneG = new Dropzone($('.addAnswerPhoto')[0], {
                url: "/product/savecommentimg",
                maxFiles:10,
                previewTemplate: previewTemplate,
                previewsContainer: "#previewsAnswerFotoContainer", 
                clickable: ".addAnswerPhoto" ,
            });

            myDropzoneG.on("maxfilesexceeded", function(file) {
            });

            myDropzoneG.on("complete", function(response) {
                if (response.status == 'success') {
                    
                    photosArray.push(response.xhr.response);
                    $('#addNewCommentAnswer').find('#productcomment-image_array').val(JSON.stringify(photosArray));
                    
                }else{
                    //$('.maxPhoto').show()
                }
            });

            myDropzoneG.on("removedfile", function(response) {
                if(response.xhr != null){
                    var removeItem = response.xhr.response;
                    photosArray = jQuery.grep(photosArray, function(value) {
                        return value != removeItem;
                    });
                    $('#addNewCommentAnswer').find('#productcomment-image_array').val(JSON.stringify(photosArray));
                }
            });
    }
    
    $(document).on('click', '.answerToComment',function(){
        var comment_id = $(this).data('comment_id');
        $('#addNewCommentAnswer').modal('show');
        $('#addNewCommentAnswer').find('#productcomment-parent_id').val(comment_id);
        
    })
    
})