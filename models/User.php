<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $password;
    public $password_repeat;
    public $profile;
    public $authKey;
    
    public static function tableName()
    {
        return 'users';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'username' => \Yii::t('app','Логін'),
            'email' => \Yii::t('app','E-mail'),
            'phone_number' => \Yii::t('app','Номер телефону'),
            'time_added' => \Yii::t('app','Дата режстрації'),
            'company_id' => \Yii::t('app','Назва компанії'),
            'city' => \Yii::t('app','Місто'),
            'address' => \Yii::t('app','Адреса'),
            'country' => \Yii::t('app','Країна'),
            'zip' => \Yii::t('app','Zip/Postal Code'),
            'first_name' => \Yii::t('app','Ім`я'),
            'last_name' => \Yii::t('app','Прізвище'),
            'type_distance' => \Yii::t('app','Одиниці виміру відстані'),
        );
    }
    
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }else {
            return self::findOne($id);
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }
    
    public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'username' => $service->getAttribute('name'),
            'authKey' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-'.$id, $attributes);
        return new self($attributes);
    }

    public static function getCompanyname($id){
        $userModel = static::find()->where(['id' => $id])->one();
        $companyModel = Company::find()->where(['id' => $userModel->company_id])->one();

        return $companyModel->urlname;
    }

    public static function getAccessToken($id){
        $userModel = static::find()->where(['id' => $id])->one();

        return $userModel->access_token;
    }

    public static function getUserAdmin($id){
        $userModel = static::find()->where(['id' => $id, 'company_id' => 0, 'users_type' => 1])->one();
        
        return $userModel;
    }
    
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email
        ]);
    }
    
    public static function getUsername($id){
        $userModel = static::find()->where(['id' => $id, 'parent_id' => 0])->one();

        if(empty($userModel)){
            $userModel = static::find()->where(['id' => $id])->one();
            $userModel = static::find()->where(['id' => $userModel->parent_id])->one();
        }
        return $userModel->username;
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'login' => ['username', 'password'],
            'signup' => ['email', 'password', 'password_hash', 'email', 'auth_key', 'type'],
            'signupAdmin' => ['username', 'password', 'password_hash', 'email', 'auth_key', 'type'],
            'update' => [ 'email', 'password', 'password_repeat', 'password_hash', 'auth_key'],
            'default' => []
        ];
    }


    public static function findByUsername($username)
    {
        return static::findOne(array('username' => $username));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($password)
    {
        //echo $password;exit;
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
//        return $this->password === $password;
    }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
            //print_r([$expire,$timestamp]);exit;
        return $timestamp + $expire >= time();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function validateName()
    {
        if(preg_match("/[a-zA-Z0-9]/", $this->username)){
            $this->addError('urlname', 'Url name must have [a-zA-Z0-9]');
        }
    }

    public function getUserinfo()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }
    
    public function rules()
    {
        return [
            [['username','email'], 'required'],
            [['username', 'email'],'unique'],
            ['username', 'string', 'min'=>5, 'max' => 32],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ], 
        ];
    }
}
