<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//address_status:  1-Show address, 0-Not show address,
//status:  1-Show, 0-Not show,
        
class ProductComment extends \yii\db\ActiveRecord
{
    public $image_array;
    
    public static function tableName()
    {
        return '{{%product_comments}}';
    }

    public function scenarios()
    {
        return [
            'new_comment' => ['product_id', 'description', 
                'user_id', 'parent_id', 'image_array'],
            'update_image' => ['img_src']
        ];
    }
    
    public function rules()
    {
        return [
             [['description'], 'required']
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    
    public function getImages()
    {
        return $this->hasMany(CommentImage::className(), ['comment_id' => 'id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getChildcomment()
    {
        return $this->hasMany($this::className(), ['parent_id' => 'id']);
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
