<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//address_status:  1-Show address, 0-Not show address,
//status:  1-Show, 0-Not show,
        
class Product extends \yii\db\ActiveRecord
{
    public $image_array;
    public $sub_category;
    public static function tableName()
    {
        return '{{%product}}';
    }

    public function scenarios()
    {
        return [
            'new_product' => [
                'name', 'user_id', 'category_id',
                'supplier', 'supplier_site', 'img_src',
                'brand', 'date_start', 'date_end',
                'about', 'price', 'product_count',
                'type', 'status', 'image_array', 'sub_category', 'short_description'
            ],
            'update_image' => ['img_src']
        ];
    }
    
    public function rules()
    {
        return [
            [['name'], 'required']
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'name' => Yii::t('app', 'Название закупки'),
            'category_id' => Yii::t('app', 'Категория товара'),
            'brand' => Yii::t('app', 'Торговая марка / бренд'),
            'supplier' => Yii::t('app', 'Поставщик'),
            'supplier_site' => Yii::t('app', 'Сайт поставщика'),
            'status' => Yii::t('app', 'Статус закупки'),
            'price' => Yii::t('app', 'Цена'),
            'about' => Yii::t('app', 'Комментарий к закупке'),
            'product_count' => Yii::t('app', 'Количество продуктов в закупе'),
            'date_start' => Yii::t('app', 'Открытие закупки'),
            'date_end' => Yii::t('app', 'Закрытие закупки'),
            'short_description' => Yii::t('app', 'Краткое описание'),
        ];
    }

}
