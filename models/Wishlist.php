<?php
namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Wishlist extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%product_wish_list}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['id_product', 'date_add'],
            'update' => ['id_product', 'date_add'],
            'delete' => ['id']
        ];
    }

    public function rules()
    {
        return [
//            [['id_product'], 'required']
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }
}
