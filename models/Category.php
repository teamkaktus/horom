<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Category extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%category}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['name', 'parent_id','img_src'],
            'update' => ['name', 'parent_id', 'img_src'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name'], 'required']
        ];
    }
    
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
