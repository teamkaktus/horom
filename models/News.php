<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//address_status:  1-Show address, 0-Not show address,
//status:  1-Show, 0-Not show,
        
class News extends \yii\db\ActiveRecord
{
    public $image_array;
    public $sub_category;
    public static function tableName()
    {
        return '{{%news}}';
    }

    public function scenarios()
    {
        return [
            'add_news' => [
                'title', 'content', 'img_src',
                'news_type', 'user_id'
            ],
            'update_news' => [
                'title', 'content', 'img_src',
            ]
        ];
    }
    
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'required'],
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
