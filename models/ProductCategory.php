<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//address_status:  1-Show address, 0-Not show address,
//status:  1-Show, 0-Not show,
        
class ProductCategory extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    public function scenarios()
    {
        return [
            'add' => [ 'product_id', 'category_id'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }
        
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }    
    
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'product_id']);
    }
//    
//    public function getImages()
//    {
//        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
//    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
        ];
    }

}
