<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Product;
use app\models\News;
use app\models\ProductComment;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class NewsController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionNewsshow($news_id)
    {
        $modelNews = News::find()->where(['id' => $news_id])->one();
        if($modelNews){
            return $this->render('show', [
                'modelNews' => $modelNews
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }
    
    public function actionAllnews()
    {
        $queryNews = News::find();
        $modelNews = new ActiveDataProvider(['query' => $queryNews, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('allnews', [
            'modelNews' => $modelNews->getModels()
        ]);
    }
       
}
