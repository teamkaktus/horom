<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Category;
use app\models\Product;
use app\models\ProductComment;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class ProductController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionCategory($type,$category_id=null)
    {
        $modelCategories = Category::find()->where(['parent_id' => '0'])->orWhere(['parent_id' => null])->all();
        
        $queryProduct = \app\models\ProductCategory::find()
                ->select(['*', 'id' => '{{product}}.id', 'product_id' => '{{product_category}}.product_id',
                'pc_category_id' => '{{product_category}}.category_id',
                'p_category_id' => '{{product}}.category_id'])
                ->joinWith('category')
                ->joinWith('product')
                ->joinWith('productImages');
        if($category_id != null){
            $queryProduct->where(['product_category.category_id' => $category_id]);
        }

                
        $page = 'category';
        switch ($type) {
            case 'hot':
                $queryProduct->andWhere(['product.type' => '0']);                
                break;
            case 'new':
                $queryProduct->andWhere(['product.type' => '0']);                
                break;
            case 'product':
                $page = 'category_product';
                $queryProduct->andWhere(['product.type' => '1']);                
                break;
        }

        $stringProductCount = $queryProduct->count();
        $modelProduct = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 12]]);

//        var_dump($modelProduct->getModels());
//        foreach(){}|
        return $this->render($page,[
            'modelProduct' => $modelProduct->getModels(),
            'pagination' => $modelProduct->pagination,
            'modelCategories' => $modelCategories,
            'stringProductCount' => $stringProductCount,
            'type' => $type,
        ]);
    }
    
    public function actionProduct($product_id=null)
    {
        if($product_id){
            $modelProduct = Product::find()->where(['id' => $product_id])->one();
            
            $modelNewComments = new ProductComment();
            $modelNewComments->scenario = 'new_comment';
            if($_POST){
                if($modelNewComments->load(\Yii::$app->request->post())){
                    if($modelNewComments->save()){
                        $arrayProductImage = json_decode($modelNewComments->image_array);
                        if($arrayProductImage){
                            foreach($arrayProductImage as $key => $stringImageSrc){
                                if($key == 0){
                                    $modelNewComments->scenario = 'update_image';
                                    $modelNewComments->img_src = $stringImageSrc;
                                    $modelNewComments->save();
                                }else{
                                    $modelNewProductImage = new \app\models\CommentImage();
                                    $modelNewProductImage->scenario = 'new_comment_image';
                                    $modelNewProductImage->img_src = $stringImageSrc;
                                    $modelNewProductImage->comment_id = $modelNewComments->id;
                                    $modelNewProductImage->save();
                                }
                            }
                        }
                        $modelNewComments = new ProductComment();
                        $modelNewComments->scenario = 'new_comment';
                        \Yii::$app->session->setFlash('comment_added');
                        return $this->redirect('/product/'.$modelProduct->id);
                    }
//                    var_dump($modelNewComments);exit;
                }
            }
            $modelComment = ProductComment::find()->where(['product_id' => $modelProduct->id, 'parent_id' => null])->all();
            
            return $this->render('product',[
                'modelProduct' => $modelProduct,
                'modelNewComments' => $modelNewComments,
                'modelComment' => $modelComment,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException(); 
        }
    }

    public function actionSavecommentimg(){
        $ds          = DIRECTORY_SEPARATOR;
        $storeFolder = \Yii::getAlias('@webroot').'/images/comment/';

        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;

            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.jpg';

            move_uploaded_file($tempFile,$targetFile);
            return \Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.jpg';
        }
    }
    
}
