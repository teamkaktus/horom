<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Product;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Wishlist;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $countOrganizer = User::find()->where(['type' => 'organizer'])->count();
        $countUser = User::find()->count();
        $countProduct = Product::find()->where(['type' => 1])->count();
        $countZacup = Product::find()->where(['type' => 0])->count();

        $modeCategory = \app\models\Category::find()->where(['parent_id' => ''])->orWhere(['parent_id' => null])->all();

        return $this->render('index', [
                'modeCategory' => $modeCategory,
                'countOrganizer' => $countOrganizer,
                'countUser' => $countUser,
                'countProduct' => $countProduct,
                'countZacup' => $countZacup,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
//    public function actionLogin()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//            'model' => $model,
//        ]);
//    }
    public function actionLogin()
    {
        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if (!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email'])) ? $profile['email'] : $profile['id']])->exists()) {
                        $model = new User();
                        $model->username = (isset($profile['name'])) ? $profile['name'] : $profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email'])) ? $profile['email'] : $profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id'])) ? $profile['id'] : '';
                        if ($model->save()) {
                            if (Yii::$app->getUser()->login($model)) {
                                $eauth->redirect();
                            }
                        }
                    } else {
                        $model = User::find()->where(['social_id' => (isset($profile['id'])) ? $profile['id'] : $profile['id'], 'username' => $profile['name'], 'email' => (isset($profile['email'])) ? $profile['email'] : $profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);
                    // special redirect with closing popup window
//                    $eauth->redirect();
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->scenario = 'loginUser';
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRules()
    {

        return $this->render('rules');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
                'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionAddwl()
    {
        $id = Yii::$app->request->post('id');
        $id_user = Yii::$app->user->identity->userinfo->id;

        if ($id != null) {
            $modelWish = Wishlist::find()->where(['id_product' => $id, 'id_user' => $id_user])->one();

            if ($modelWish == null) {

                $customer = new Wishlist();
                $customer->scenario = 'add';
                $customer->id_product = $id;
                $customer->id_user = $id_user;

                if ($customer->save()) {
                    echo 'good';
                } else {
                    echo 'bad';
                }
            } else {
                echo 'уже є у вашому списку';
            }
        } else {
            echo 'я хз, як так';
        }

        exit;
    }

    public function actionRemovewl()
    {
        $id = Yii::$app->request->post('id');
        $id_user = Yii::$app->user->identity->userinfo->id;

        if ($id != null) {
            $modelWish = Wishlist::find()->where(['id' => $id])->one();

            if ($modelWish != null) {

           
                if ($modelWish->delete()) {
                    echo 'good';
                } else {
                    echo 'bad';
                }
            } else {
                echo 'уже нема у вашому списку';
            }
        } else {
            echo 'я хз, як так';
        }
        exit;
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
                'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::home());
        }
        $model = new User();
        $modelNewUserInfo = new \app\models\UserInfo();
        $request = Yii::$app->request;
        $postedForm = $request->post('ContactForm');
        $model->scenario = 'signup';
        if ($model->load($request->post())) {
            $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = 'key';
            if ($model->save()) {
                $modelNewUserInfo = new \app\models\UserInfo();
                $modelNewUserInfo->scenario = 'signup';
                $modelNewUserInfo->user_id = $model->id;
                $modelNewUserInfo->username = $request->post('UserInfo')['username'];
                $modelNewUserInfo->save();
                if (Yii::$app->getUser()->login($model)) {
                    return $this->redirect(Url::home());
                }
            }
        }

        return $this->render('signup', [
                'newModel' => $model,
                'modelNewUserInfo' => $modelNewUserInfo,
        ]);
    }

    public function actionGetsubcategory()
    {
        $category_id = $_POST['category_id'];
        $modelCategories = \app\models\Category::find()->where(['parent_id' => $category_id])->asArray()->all();
        echo json_encode($modelCategories);
    }
}
