<?php
namespace app\widgets;
use app\models\User;
class UseroncatalogWidget extends \yii\bootstrap\Widget
{
    public $user_id;    
    public $type;    
    public function init(){}

    public function run() {
        $view = 'user/useroncatalogtovar';
        if($this->type == 'zacup'){
            $view = 'user/useroncatalog';            
        }
        return $this->render($view, [
            'modelUser' => User::find()->where(['id' => $this->user_id])->one(),
        ]);
    }
}
