<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<div class="organizator_conteinar_product_in_stock">
    <div class="title_organizator_product_in_stock"><span>Организатор</span></div>
    <div class="image_container_organizator_product_in_stock">
        <?php $avatar = '/images/default_avatar.jpg'; ?>
            <?php if($modelUser->userinfo->avatar != ''){ ?>
                <?php $avatar = '/'.$modelUser->userinfo->avatar; ?>
            <?php } ?>
        <img class="image_organizator_product_in_stock" src="<?= $avatar; ?>">
    </div>
    <div class="name_container_organizator_product_in_stock">
        <span class="name_organizator_product_in_stock"><?= $modelUser->userinfo->username; ?></span>
    </div>
</div>
<div class="rating_conteinar_product_in_stock">
    <div class="title_container_rating_product_in_stock">
        <span class="title_rating_product_in_stock">Рейтинг -</span>
        <img class="like_icon_product_in_stock" src="/img/like_icon.png">
        <span class="number_rating_product_in_stock">251</span>
    </div>
</div>
