<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="container_organizator_tovara_burning-purchases2">
    <div class="container_name_organizator_tovara_burning-purchases">
        <span class="name_organizator_tovara_burning-purchases">Организатор закупки</span>
        <span class="name_organizator_tovara_burning-purchases2">Рейтинг -</span>
        <img class="like_icon_product_in_stock" src="/img/like_icon.png">
        <span class="number_rating_product_in_stock">251</span>
    </div>
    <div class="container_image_and_name_burning-purchases">
        <div class="image_container_organizator_product_in_stock">
            <?php $avatar = '/images/default_avatar.jpg'; ?>
            <?php if($modelUser->userinfo->avatar != ''){ ?>
                <?php $avatar = '/'.$modelUser->userinfo->avatar; ?>
            <?php } ?>
            <img class="image_organizator_product_in_stock" src="<?= $avatar; ?>">
        </div>
        <div class="name_container_organizator_burning-purchases">
            <span class="name_organizator_burning-purchases"><?= $modelUser->userinfo->username; ?></span>
        </div>
    </div>
</div>