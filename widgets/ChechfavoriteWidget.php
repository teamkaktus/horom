<?php
namespace app\widgets;
use app\models\User;
use app\models\Wishlist;
class ChechfavoriteWidget extends \yii\bootstrap\Widget
{
    public $product_id;
    public function init(){}

    public function run() {
        if(!\Yii::$app->user->isGuest){
            $modelWishlist = Wishlist::find()->where(['id_product' => $this->product_id, 'id_user' => \Yii::$app->user->id])->one();
           
            if($modelWishlist != null){
                return '<i class="whish_list_star_whish glyphicon glyphicon-star whish_list_star" onclick="remove_to_whish_list('.$modelWishlist->id.','.$this->product_id.',this)"></i>';
            }else{
                return '<i class="glyphicon glyphicon-star whish_list_star" onclick="add_to_whish_list('.$this->product_id.',this)"></i>';
            }
        }

    }
}
